#!/bin/bash
#BSUB -J sleeper4
#BSUB -o sleeper4_%J.out
#BSUB -q hpcintro
#BSUB -R "rusage[mem=512MB]"
#BSUB -W 00:10
#BSUB -n 32
#BSUB -R "span[ptile=1]"
#BSUB -B
#BSUB -N 
#BSUB -R "select[model == XeonGold6126]"
sleep 10

