#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "datatools.h"		/* helper functions	        */
#include "matmult.h"		/* my matrix add fucntion	*/

#define NREPEAT 100		/* repeat count for the experiment loop */

#define mytimer clock
#define delta_t(a,b) (1e3 * (b - a) / CLOCKS_PER_SEC)

int
main(int argc, char *argv[]) {

    int    i, m, n, k, N = NREPEAT;
    double **A, **B, **C;
    double tcpu1; 

    clock_t t1, t2;

    for (m = 200; m <= 350; m += 10) {
	n = m + 25;
	k = m + 50;

	/* Allocate memory */
	A = dmalloc_2d(m, k);
	B = dmalloc_2d(k, n);
	C = dmalloc_2d(m, n);
	if (A == NULL || B == NULL | C == NULL) {
	    fprintf(stderr, "Memory allocation error...\n");
	    exit(EXIT_FAILURE);
	}

	/* initialize with useful data - last argument is reference */
	init_data(m,n,k,A,B);


	/* timings for matadd */
	t1 = mytimer();
	for (i = 0; i < N; i++)
		matmult(m, n, k, A, B, C);
	t2 = mytimer();
	tcpu1 = delta_t(t1, t2) / N;

	// check_results("main", m, n, c);

	/* Print n and results  */
	printf("%4d %4d %8.3f\n", m, n, tcpu1);

	/* Free memory */
	dfree_2d(A);
	dfree_2d(B);
	dfree_2d(C);
    }

    return EXIT_SUCCESS;
}
