#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "datatools.h"		/* helper functions	        */
#include "matmult.h"		/* my matrix add fucntion	*/

#define NREPEAT 100		/* repeat count for the experiment loop */

#define mytimer clock
#define delta_t(a,b) (1e3 * (b - a) / CLOCKS_PER_SEC)

int
main(int argc, char *argv[]) {

    int    i, m, n, N = NREPEAT;
    double **A, *b, *c;
    double tcpu1; 

    clock_t t1, t2;

    for (m = 200; m <= 3500; m += 300) {
	n = m + 25;

	/* Allocate memory */
	A = malloc_2d(m, n);
	b = malloc(n*sizeof(double));
	c = malloc(n*sizeof(double));
	if (A == NULL || b == NULL | c == NULL) {
	    fprintf(stderr, "Memory allocation error...\n");
	    exit(EXIT_FAILURE);
	}

	/* initialize with useful data - last argument is reference */
	init_data(m,n,A,b);


	/* timings for matadd */
	t1 = mytimer();
	for (i = 0; i < N; i++)
	    matmult(m, n, A, b, c);
	t2 = mytimer();
	tcpu1 = delta_t(t1, t2) / N;

	// check_results("main", m, n, c);

	/* Print n and results  */
	printf("%4d %4d %8.3f\n", m, n, tcpu1);

	/* Free memory */
	free_2d(A);
	free(b);
	free(c);
    }

    return EXIT_SUCCESS;
}
