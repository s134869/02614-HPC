void
matmult(int m, int n, double **A, double *b, double *c) {
    
    int i, j;

    for(i = 0; i < m; i++)
	for(j = 0; j < n; j++)
	    c[i] = A[i][j] * c[j];
}
