#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J Permutation_job_small

blk="2 5 10 20 30 60 70 90 120 150 200 500 1000 2000"
LOGEXT=$CC.dat

let m = 1000
# Create results folder



if [ ! -d ./block_comp ]; then
  mkdir -p  ./block_comp;
fi

for m in $M
do
    let n=m+20
    let k=n+50
    ./matmult_c.gcc blk $m $k $n | grep -v CPU >> ./block_comp$LOGEXT
    done
done    

exit 0

