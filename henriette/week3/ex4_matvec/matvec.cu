#include <cuda_runtime.h>

__global__ void matvec(int m, int n, double *A, double *b, double *c) {
    int col, row;
    
    col = blockIdx.x * blockDim.x + threadIdx.x; 
    row = blockIdx.y * blockDim.y + threadIdx.y;

    c[row] += A[row*n + col] * b[col];
}

