#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "datatools.h"
#include "matvec.h"
#include <helper_cuda.h>
#include <math.h>
#include "cublas_v2.h"

#define BLOCK_SIZE 16
#define GRID_SIZE 1
#define max_threads = 1024

/**
 * Rewriting matrix vector multiplication for CUDA execution
 * @author frksteenhoff
 */

int main(int argc, char *argv[]) {
    double *h_A, *d_A, *d_b, *h_b, *h_c, *d_c; // vector

    int row_size = 40; // Should be command line args
    int col_size = 40;

    int vec_size = row_size * sizeof(double); // Number of bytes needed
    int mat_size = row_size * col_size * sizeof(double);

    // Setting dimensions for kernel
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid(GRID_SIZE, GRID_SIZE);

    // Time
    fflush(stdout);
    double time = omp_get_wtime();
  
    // Allocate memory host
    cudaMallocHost((void **) &h_A, mat_size);
    cudaMallocHost((void **) &h_b, vec_size);
    cudaMallocHost((void **) &h_c, vec_size);

    // Init host data
    init_data_vec(row_size * col_size, h_A);
    init_data_vec(row_size, h_b);
    init_data_vec(row_size, h_c);

    // Allocate memory device
    cudaMalloc((void **) &d_A, mat_size);    
    cudaMalloc((void **) &d_b, vec_size);    
    cudaMalloc((void **) &d_c, vec_size);    

    // ----- Kernel launch -----
    //matvec<<<dimGrid, dimBlock>>>(row_size, col_size, d_A, d_b, d_c);
    //checkCudaErrors(cudaDeviceSynchronize());
    
    // Copy back from device to host
    cudaMemcpy(h_A, d_A, mat_size, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_b, d_b, vec_size, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_c, d_c, vec_size, cudaMemcpyDeviceToHost);

    // Free memory host
    cudaFreeHost(h_A);
    cudaFreeHost(h_b);
    cudaFreeHost(h_c);

    // Free memory device
    cudaFree(d_A);
    cudaFree(d_b);
    cudaFree(d_c);

    // Print time to completion
    printf("\n--------\ntime = %3.2f seconds\n", omp_get_wtime() - time);
    return(0);
}
