#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "datatools.h"
#include "matvec.h"
#include <helper_cuda.h>
#include <math.h>
#include "cublas_v2.h"

#define BLOCK_SIZE 16
#define GRID_SIZE 1
#define max_threads = 1024

/**
 * Rewriting matrix vector multiplication for CUDA execution
 * @author frksteenhoff
 */

int main(int argc, char *argv[]) {
    double *h_A, *d_A, *d_b, *h_b;
    double *h_c, *d_c;

    int row_size = 40; // Should be command line args
    int col_size = 40;
    cublasStatus_t stat;
    cublasHandle_t handle;

    int mat_size = row_size * col_size * sizeof(double);

    // Setting dimensions for kernel
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid(GRID_SIZE, GRID_SIZE);

    // Time
    fflush(stdout);
    double time = omp_get_wtime();
  
    // Allocate memory host
    cudaMallocHost((void **) &h_A, mat_size);
    cudaMallocHost((void **) &h_b, mat_size);
    cudaMallocHost((void **) &h_c, mat_size);

    // Init host data
    init_data_vec(row_size * col_size, h_A);
    init_data_vec(row_size * col_size, h_b);
    init_data_vec(row_size * col_size, h_c);

    // Allocate memory device
    cudaMalloc((void **) &d_A, mat_size);    
    cudaMalloc((void **) &d_b, mat_size);    
    cudaMalloc((void **) &d_c, mat_size);    


    stat = cublasCreate(&handle);
    // ----- Kernel launch -----
    double a = 1.0;
    double b = 0.0;
    int          k     = 1;
    const double *alpha = &a;
    int          lda   = 0;
    const double *beta = &b;
    int          ldb   = 0;
    int          ldc   = 0;
    cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, 
                row_size, col_size, k, 
                alpha, d_A, lda, 
                d_b, ldb, beta, 
                d_c, ldc);
    checkCudaErrors(cudaDeviceSynchronize());
    

    // Copy back from device to host
    cudaMemcpy(h_A, d_A, mat_size, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_b, d_b, mat_size, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_c, d_c, mat_size, cudaMemcpyDeviceToHost);

    // Free memory host
    cudaFreeHost(h_A);
    cudaFreeHost(h_b);
    cudaFreeHost(h_c);

    // Free memory device
    cudaFree(d_A);
    cudaFree(d_b);
    cudaFree(d_c);

    // Print time to completion
    printf("\n--------\ntime = %3.2f seconds\n", omp_get_wtime() - time);
    return(0);
}
