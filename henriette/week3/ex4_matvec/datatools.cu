#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include "cublas_v2.h"


// Using version fromm Bernd's datatools.c file

void init_data_vec(int m, double *A) {

   int i;

   for( i = 0; i < m; i++)
	    A[i] = 1.0;		    
}


void init_data_single(int m, int n, double **A) {

   int i, j;

   for( i = 0; i < m; i++)
       for( j = 0; j < n; j++) {
	    A[i][j] = 1.0;	    
       }
}


void init_data_single_empty(int m, int n, double **A) {
    int i, j;

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            A[i][j] = 0;
}

void free_2d(double **A) {
    free(A[0]);
    free(A);
}

