#include "cublas_v2.h"

double **malloc_2d(int m, int n);
void free_2d(double **A);
void print_matrix_to_file(int N,double **M , int type);
void init_data_vec(int m, const double *A);
void init_data_single(int m, int n, double **A);
void init_data_single_empty (int m, int n, double **A);
