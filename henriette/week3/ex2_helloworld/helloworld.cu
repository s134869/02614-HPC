#include <omp.h>
#include <stdio.h>
#include <helper_cuda.h>

/**
 * My first program in CUDA
 * @author frksteenhoff
 */
const int device = 0;

__global__ void helloworld(){
   printf("Hello world! I'm thread %d out of %d in block %d. My global thread ID is %d.\n", threadIdx.x, blockDim.x, blockIdx.x, (blockIdx.x * blockDim.x + threadIdx.x));
}

int main(int argc, char *argv[])
{
    // Wake up GPU from power save state.
    printf("Warming up device %i ... \n", device); 
    fflush(stdout);

    double time = omp_get_wtime();
    cudaSetDevice(device);           // Set the device to 0 or 1.
    
    // Kernel lauch
    int numBlocks = 2;
    int numThreadsPrBlock = 10; 
    helloworld<<<numBlocks, numThreadsPrBlock>>>();
    checkCudaErrors(cudaDeviceSynchronize());

    printf("time = %3.2f seconds\n", omp_get_wtime() - time); // Print time to completion
}
