#include <stdio.h>
#include <stdlib.h>
#include "mandel.h"
#include "writepng.h"

/**
 * Rewriting the mandelbrot problem for CUDA
 * @author frksteenhoff
 */
const int device = 0;

int main(int argc, char *argv[]) {
    fflush(stdout);
    double time = omp_get_wtime();

    cudaSetDevice(device);           // Set the device to 0 or 1.
    int   width, height;
    int	  max_iter;
    int   *image;

    width    = 400;
    height   = 400;
    max_iter = 400;
    int i;

    // Allocate memory host and device
    // command line argument sets the dimensions of the image
    if (argc == 2) width = height = atoi(argv[1]);

    image = (int *)malloc( width * height * sizeof(int));
    cudaMalloc((int *)malloc( width * height * sizeof(int)));
    if (image == NULL) {
       fprintf(stderr, "memory allocation failed!\n");
       return(1);
    }

    // Transfer data from host to device
    //cudaMemcpy(); 

    // Kernel launch
    mandel<<<width, height>>>(width, height, image, max_iter);
    checkCudaErrors(cudaDeviceSynchronize());

    //for(i = 0 ; i < 1000 ; i++)
    //    mandel(width, height, image, max_iter);

    writepng("mandelbrot.png", image, width, height);

    printf("time = %3.2f seconds\n", omp_get_wtime() - time); // Print
    
    free(image);
    cudaFree(image);
    return(0);
}

__global__ void mandel(int disp_width, int disp_height, int *array, int max_iter) {

    double 	scale_real, scale_imag;
    double 	x, y, u, v, u2, v2;
    int 	i, j, iter;

    scale_real = 3.5 / (double)disp_width;
    scale_imag = 3.5 / (double)disp_height;

    for(i = 0; i < disp_width; i++) {
	x = ((double)i * scale_real) - 2.25; 
	for(j = 0; j < disp_height; j++) {
		//printf("%d" , omp_get_max_threads())
		y = ((double)j * scale_imag) - 1.75; 
		u    = 0.0;
		v    = 0.0;
		u2   = 0.0;
		v2   = 0.0;
		iter = 0;
	
		while ( u2 + v2 < 4.0 &&  iter < max_iter ) {
			v = 2 * v * u + y;
			u = u2 - v2 + x;
			u2 = u*u;
			v2 = v*v;
			iter = iter + 1;
		}
		// if we exceed max_iter, reset to zero
		iter = iter == max_iter ? 0 : iter;

		array[i*disp_height + j] = iter;
	}
    }
}
