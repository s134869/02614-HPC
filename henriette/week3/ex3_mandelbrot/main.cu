#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "mandelgpu.h"
#include "writepng.h"
#include <helper_cuda.h>
#define N 30
#define max_threads = 1024

/**
 * Rewriting the mandelbrot problem for CUDA execution
 * @author frksteenhoff
 */

int main(int argc, char *argv[]) {
    int   width, height;
    int	  max_iter;
    int   *d_image, *h_image;

    width    = 400;
    height   = 400;
    max_iter = 400;

    dim3 dimGrid(64,40,1);
    dim3 dimBlock(10,10,10);

    int size = width * height * sizeof(int);

    fflush(stdout);
    double time = omp_get_wtime();
   
    // Allocate memory host and device
    cudaMallocHost((void **) &h_image, size);
    cudaMalloc((void **) &d_image, size);    

    if (d_image == NULL) {
       fprintf(stderr, "memory allocation failed!\n");
       return(1);
    }

    // Kernel launch
    mandelgpu<<<dimGrid, dimBlock>>>(width, height, d_image, max_iter);
    checkCudaErrors(cudaDeviceSynchronize());

    // Copy back
    cudaMemcpy(h_image, d_image, size, cudaMemcpyDeviceToHost);

    writepng("mandelbrot.png", h_image, width, height);

    // Free memory
    cudaFreeHost(h_image);
    cudaFree(d_image);

    printf("time = %3.2f seconds\n", omp_get_wtime() - time); // Print time to completion
    return(0);
}

