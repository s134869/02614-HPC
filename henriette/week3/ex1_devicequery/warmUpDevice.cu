#include <omp.h>
#include <stdio.h>

const int device = 0;

__global__ void helloworld(){
   printf("Hello world!");
}

int main(int argc, char *argv[])
{
    // Wake up GPU from power save state.
    printf("Warming up device %i ... ", device); fflush(stdout);
    double time = omp_get_wtime();
    cudaSetDevice(device);           // Set the device to 0 or 1.
    double *dummy_d;
    cudaMalloc((void**)&dummy_d, 0); //

    // Kernel lauch
    helloworld<<<1, 1>>>();
    cudaDeviceSynchronize();


    printf("time = %3.2f seconds\n", omp_get_wtime() - time);
}
