/*
void main(int m, int n, double *a,
	double *b, double *c) {

	int i, j;
	double sum;

	for (i=0; i<m; i++) {
		sum = 0.0;
		for (j=0; j<n; j++) {
			sum += b[i*n+j]*c[j];	
		}
		a[i] = sum;
	}
}*/

void main(int m, int n, double *a,
	double **b, double *c) {

	int i, j, k;
	double sum;
	int N = 1000000000;
	
	for(k = 0; k < N; k++) {
	    	#pragma omp parallel for default(none) shared(m,n,a,b,c) \
		private(i,j,sum)        

		for (i=0; i<m; i++) {
			sum = 0.0;
			for (j=0; j<n; j++) {
				sum += b[i][j]*c[j];	
			}
			a[i] = sum;
		}
    }
}
