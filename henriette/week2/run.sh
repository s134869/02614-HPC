#!bin/bash
M="10 50 100 200 500 750 1000"
LOGEXT=$CC.dat

# Create results folder

if [ ! -d ./res ]; then
  mkdir -p  ./res;
fi

  
for m in $M
do
    let n=m+20
    let k=n+50
    ./matmult_c.gcc nat | grep -v CPU >> ./res/$perm$LOGEXT
done

exit 0
