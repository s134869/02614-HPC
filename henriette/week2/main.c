/* Function evaluating approximate value of Pi */
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <stdlib.h>

#include "pifunc.c"
#include "matvec.c"
#include "datatools.h"


int main(int argc, char *argv[]) {	
        double time, pi;
	double *a, *c; 
	double **b; 
        int N, val, m, n, i;
	bool choice = false;

        
	time_t t1;
        t1 = clock();
	
	if(choice) {
	    m = 200;
   	    double values[m];
	    n = 225;
	    a = values; // same as allocation
	    //b = malloc_2d(m,n);
	    c = malloc(m * sizeof(double *));
	    for(i = 0; i < m; i++) {
	    	c[i] = i;
	    }
	    
	    matvec(m, n, a, b, c);
	    //matvec(m, n, a, b, c);
	} else {       
	    N = 10000;
	    //pi = pi_func(N);
	    pi_func_omp(N);
	    printf("num: %f, pi: %f\n", pi, N);
	    printf("time: %f secs\n", time);

        }
	time = (clock() - t1)/(double) CLOCKS_PER_SEC;
	return (0);
}
