#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include "fib.c"

int main(int argc, char *argv[]) {
    int input = atoi(argv[1]); // atoi = ascii to int
    int output;
    
    output = fib(input);
    printf("The %d. Fibonacci number is: %d\n", input, output);

    return(0);
}
