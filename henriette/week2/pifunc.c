/* Calculate pi 

void pi_func(int N) {
	double pi = 0;
	int i;

	for(i = 0; i < N; i++) {
		pi += 4.0 / (1.0 + ((i - 0.5) / N) * ((i - 0.5) / N));
	}
	pi = 1/N * pi;
}*/

void main() {
	int N = 1000000000;
	double pi = 0;
	int i;
	#pragma omp parallel for default(none) shared(N) private(i) reduction(+: pi)
	for(i = 0; i < N; i++) {
		pi += 4.0 / (1.0 + ((i - 0.5) / N) * ((i - 0.5) / N));
	}
	pi = 1/N * pi;
}
