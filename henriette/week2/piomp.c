void main(){
int i;
int N = 1000000000;
double pi = 0;

#pragma omp parallel for default(none) shared(N) private(i) reduction(+: pi)

for( i=1 ; i<=N ; i++) {
    pi += 4.0 / ( 1.0 + ((i-0.5) / N) * ((i-0.5) / N) );
}

pi = pi * 1/N;
}
