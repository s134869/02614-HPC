#include <helper_cuda.h>
#include "cublas_v2.h"
#include <stdio.h>

#include <omp.h>

#include <device_launch_parameters.h>

#include <cuda_runtime_api.h>

#define VERTICAL 1
#define HORIZONTAL 0
#define BLOCK_SIZE 16
#define REGISTER_SIZE 8

#define REPETITIONS 1

extern "C"
{
#include <cblas.h>
    void matmult_lib(int m, int n, int k, double *A, double *B, double *C)
    {
        // Input:
        //      Integers m, n, k (matrix dimensions)
        //      Matrix A (m x k)
        //      Matrix B (k x n)
        // Output:
        //      Matrix C: m x n

        double t;
        int l;

        t = omp_get_wtime();
        for (l = 0; l < REPETITIONS; l++)
        {
            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, A, k, B, n, 0.0, C, n);
        }
        // printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));
        
    }
}

__global__ void kernel_v1(int m, int n, int k, double *A, double *B, double *C)
{
    int i, j, l;

    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            C[i * n + j] = 0;
            for (l = 0; l < k; l++)
            {
                C[i * n + j] += A[i * k + l] * B[l * n + j];
            }
        }
    }
}

__global__ void kernel_v2(int m, int n, int k, double *A, double *B, double *C)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i < m && j < n)
    {
        int l;
        double sum = 0;
        for (l = 0; l < k; l++)
        {
            sum += A[i * k + l] * B[l * n + j];
        }
        C[i * n + j] = sum;
    }
}

__global__ void kernel_v3_h(int m, int n, int k, double *A, double *B, double *C)
{
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = 2 * (blockIdx.x * blockDim.x + threadIdx.x);

    if (i < m && (j + 1) < n)
    {
        int l;
        double sum1 = 0.0;
        double sum2 = 0.0;
        for (l = 0; l < k; l++)
        {
            // Thread and right neighbor
            sum1 += A[i * k + l] * B[l * n + j];
            sum2 += A[i * k + l] * B[l * n + (j + 1)];
        }

        // Insert value in global memory
        C[i * n + j] = sum1;
        C[i * n + (j + 1)] = sum2;
    }

    else if (i < m && j < n)
    {
        int l;
        double sum1 = 0;
        for (l = 0; l < k; l++)
        {
            // Thread and right neighbor
            sum1 += A[i * k + l] * B[l * n + j];
        }

        // Insert value in global memory
        C[i * n + j] = sum1;
    }
}

__global__ void kernel_v3_v(int m, int n, int k, double *A, double *B, double *C)
{
    int i = 2 * (blockIdx.y * blockDim.y + threadIdx.y);
    int j = blockIdx.x * blockDim.x + threadIdx.x;

    double sum1 = 0.0;
    double sum2 = 0.0;

    if ((i + 1) < m && j < n)
    {
        int l;
        for (l = 0; l < k; l++)
        {
            sum1 += A[i * k + l] * B[l * n + j];
            sum2 += A[(i + 1) * k + l] * B[l * n + j];
        }

        // Insert value in global memory
        C[i * n + j] = sum1;
        C[(i + 1) * n + j] = sum2;
    }

    else if (i < m && j < n)
    {
        int l;
        double sum1 = 0;
        for (l = 0; l < k; l++)
        {
            sum1 += A[i * k + l] * B[l * n + j];
        }
        C[i * n + j] = sum1;
    }
}

__global__ void kernel_shared(int m, int n, int k, double *A, double *B, double *C)
{
    int b, p;

    // Find thread location

    // Full location in matrix
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;

    // Which block
    int block_col = blockIdx.x;
    int block_row = blockIdx.y;
    //printf("Block (row , col): (%d , %d)\n" , block_row , block_col);

    // Where in block
    int col_in_block = threadIdx.x;
    int row_in_block = threadIdx.y;

    double C_val = 0.0;

    int N_block_col_A = k / BLOCK_SIZE;
    //int N_block_row_B = k / BLOCK_SIZE;
    //int N_block_row_A = m / BLOCK_SIZE;
    //int N_block_col_B = n / BLOCK_SIZE;

    // printf("Number of blocks: (%d , %d , %d , %d)\n" , N_block_row_A , N_block_col_A , N_block_row_B , N_block_col_B);

    int block_corner_A;
    int block_corner_B;

    // Total number of elements in a block
    //int elements_pr_block = BLOCK_SIZE * BLOCK_SIZE;

    // printf("Corners of block (%d , %d) is %d where gridDimX is: %d\n" , block_row , block_col , elements_pr_block * block_col +  elements_pr_block * block_row * gridDim.x , gridDim.x);

    // Consider submatrix
    // double * C_block = &C[ elements_pr_block * block_col +  elements_pr_block * block_row * gridDim.y];

    // Loop over A and B blocks
    for (b = 0; b < N_block_col_A; b++)
    {
        //printf("Loop");
        //for(b = 1 ; b < 2 ; b++){

        // Allocate memory for A and B block
        __shared__ double sharedA[BLOCK_SIZE][BLOCK_SIZE];
        __shared__ double sharedB[BLOCK_SIZE][BLOCK_SIZE];

        // Upper left corner of blocks
        block_corner_A = block_row * k * BLOCK_SIZE + b * BLOCK_SIZE;
        block_corner_B = block_col * BLOCK_SIZE + b * n * BLOCK_SIZE;

        //printf("Corners of block A (%d , %d) is %d\n" , block_row , block_col , elements_pr_block * block_col +  elements_pr_block * block_row * gridDim.y);

        //printf("For thread block (%d , %d) we have block corner A: %d with b = %d\n", block_row , block_col , block_corner_A , b);
        //printf("For thread block (%d , %d) we have block corner B: %d with b = %d\n", block_row , block_col , block_corner_B , b);

        // Load memory into the shared memories
        sharedA[row_in_block][col_in_block] = A[block_corner_A + row_in_block * k + col_in_block];
        sharedB[row_in_block][col_in_block] = B[block_corner_B + row_in_block * n + col_in_block];

        //printf("A: For thread block (%d , %d) we have in-block-tread (%d , %d) with shared coord (%d) with b = %d\n", block_row , block_col , row_in_block , col_in_block , block_corner_A + row_in_block * k + col_in_block , b);
        //printf("For thread block (%d , %d) we have block corner A: %d with b = %d\n", block_row , block_col , block_corner_A , b);

        // Everything is writed to shared memory before loading data
        __syncthreads();

        // Compute contirbution to C-thread within the given block
        for (p = 0; p < BLOCK_SIZE; p++)
        {
            C_val += sharedA[row_in_block][p] * sharedB[p][col_in_block];
            //printf("For thread block (%d,%d) whint int coords (%d , %d) and b = %d we have A (%d , %d) and B (%d , %d)\n", block_row , block_col , row_in_block , col_in_block , b , row_in_block , p , p , col_in_block);
        }
        // Make sure all threads are done before reloading shared memory block
        __syncthreads();
    }
    C[i * n + j] = C_val;
}

__global__ void kernel_v4(int m, int n, int k, double *A, double *B, double *C)
{
    double sum[REGISTER_SIZE] = {0.0, 0.0 , 0.0, 0.0 , 0.0, 0.0 , 0.0, 0.0};
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;

    i = VERTICAL ? i * REGISTER_SIZE : i;
    j = HORIZONTAL ? i * REGISTER_SIZE : j;

    if (i < m && j < n)
    {
        int l, b;
        int remainder = m - i > 0 ? m - i : 0;
        int blocks = remainder < REGISTER_SIZE ? remainder : REGISTER_SIZE;
        for (l = 0; l < k; l++)
            for (b = 0; b < blocks; b++)
                sum[b] += A[(i + VERTICAL * b) * k + l] * B[l * n + (j + HORIZONTAL * b)];

        // Insert value(s) in global memory
        for (b = 0; b < blocks; b++)
            C[(i + VERTICAL * b) * n + (j + HORIZONTAL * b)] = sum[b];
    }
}


extern "C"
{

    void matmult_gpu1(int m, int n, int k, double *h_A, double *h_B, double *h_C)
    {
        double *d_A, *d_B, *d_C;
        long A_size = m * k * sizeof(double);
        long B_size = k * n * sizeof(double);
        long C_size = m * n * sizeof(double);

        double t;
        int l;

        // Cuda Malloc
        cudaMalloc((void **)&d_A, A_size);
        cudaMalloc((void **)&d_B, B_size);
        cudaMalloc((void **)&d_C, C_size);

        // Transfer host data data to device
        cudaMemcpy(d_A, h_A, A_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B, B_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_C, h_C, C_size, cudaMemcpyHostToDevice);

        t = omp_get_wtime();
        for (l = 0; l < REPETITIONS; l++)
        {
            kernel_v1<<<1, 1>>>(m, n, k, d_A, d_B, d_C);
            cudaDeviceSynchronize();
        }
        printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));

        cudaMemcpy(h_C, d_C, C_size, cudaMemcpyDeviceToHost);

        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
}

extern "C"
{
#include <cblas.h>
    void matmult_gpulib(int m, int n, int k, double *h_A, double *h_B, double *h_C)
    {
        cudaSetDevice(1);
        
        double *d_A, *d_B, *d_C;

        long A_size = m * k * sizeof(double);
        long B_size = k * n * sizeof(double);
        long C_size = m * n * sizeof(double);

        long double t;
        int l;

        cublasHandle_t handle;

        // Allocate memory device
        cudaMalloc((void **)&d_A, A_size);
        cudaMalloc((void **)&d_B, B_size);
        cudaMalloc((void **)&d_C, C_size);

        cudaMemcpy(d_A, h_A, A_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B, B_size, cudaMemcpyHostToDevice);

        cublasCreate(&handle);

        double alpha = 1.0;
        double beta = 0.0;
        //const double *alpha = &a;
        int lda = k;
        //const double *beta = &b;
        int ldb = n;
        int ldc = n;

        // ----- Kernel launch -----
        t = omp_get_wtime();
        for (l = 0; l < 1; l++)
        {
            cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, d_B, ldb, d_A, lda, &beta, d_C, ldc);
        }
        // printf("%Lf ", (omp_get_wtime() - t) );

        // Copy back from device to host
        cudaMemcpy(h_C, d_C, C_size, cudaMemcpyDeviceToHost);

        // Free memory device
        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
}

extern "C"
{
    void matmult_gpu2(int m, int n, int k, double *h_A, double *h_B, double *h_C)
    {
        double *d_A, *d_B, *d_C;
        long A_size = m * k * sizeof(double);
        long B_size = k * n * sizeof(double);
        long C_size = m * n * sizeof(double);

        double t;
        int l;

        // Cuda Malloc
        cudaMalloc((void **)&d_A, A_size);
        cudaMalloc((void **)&d_B, B_size);
        cudaMalloc((void **)&d_C, C_size);

        // Transfer host data data to device
        cudaMemcpy(d_A, h_A, A_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B, B_size, cudaMemcpyHostToDevice);
        // cudaMemcpy(d_C, h_C, C_size, cudaMemcpyHostToDevice);

        // Set gridsize
        int gridWidth = 1 + n / BLOCK_SIZE;
        int gridHeight = 1 + m / BLOCK_SIZE;

        // printf("Grid: %d x %d", gridWidth, gridHeight);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 dimGrid(gridWidth, gridHeight, 1);

        t = omp_get_wtime();
        for (l = 0; l < REPETITIONS; l++)
        {
            kernel_v2<<<dimGrid, dimBlock>>>(m, n, k, d_A, d_B, d_C);
            cudaDeviceSynchronize();
        }
        // printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));

        cudaMemcpy(h_C, d_C, C_size, cudaMemcpyDeviceToHost);
        //int threads = gridHeight * gridWidth * BLOCK_SIZE * BLOCK_SIZE;
        // printf("Grid: %d x %d (threads = %d)\n", gridWidth, gridHeight, threads);

        // cudaFree(d_A);
        // cudaFree(d_B);
        // cudaFree(d_C);
    }
}
extern "C"
{
    void matmult_gpu3(int m, int n, int k, double *h_A, double *h_B, double *h_C)
    {
        cudaSetDevice(1);
        double *d_A, *d_B, *d_C;
        long A_size = m * k * sizeof(double);
        long B_size = k * n * sizeof(double);
        long C_size = m * n * sizeof(double);

        double t;
        int l;

        // Cuda Malloc
        cudaMalloc((void **)&d_A, A_size);
        cudaMalloc((void **)&d_B, B_size);
        cudaMalloc((void **)&d_C, C_size);

        // Transfer host data data to device
        cudaMemcpy(d_A, h_A, A_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B, B_size, cudaMemcpyHostToDevice);
        // cudaMemcpy(d_C, h_C, C_size, cudaMemcpyHostToDevice);

        // Set gridsizehalve the width/height depending on the flags set
        int gridWidth, gridHeight;
        gridWidth = HORIZONTAL ? (1 + n / BLOCK_SIZE / 2) : (1 + n / BLOCK_SIZE);
        gridHeight = VERTICAL ? (1 + m / BLOCK_SIZE / 2) : (1 + m / BLOCK_SIZE);

        // printf("Grid: %d x %d", gridWidth, gridHeight);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 dimGrid(gridWidth, gridHeight, 1);

        if (VERTICAL)
        {
            t = omp_get_wtime();
            for (l = 0; l < REPETITIONS; l++)
            {
                kernel_v3_v<<<dimGrid, dimBlock>>>(m, n, k, d_A, d_B, d_C);
                cudaDeviceSynchronize();
            }
            // printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));
        }
        else if (HORIZONTAL)
        {
            t = omp_get_wtime();
            for (l = 0; l < REPETITIONS; l++)
            {
                kernel_v3_h<<<dimGrid, dimBlock>>>(m, n, k, d_A, d_B, d_C);
                cudaDeviceSynchronize();
            }
            // printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));
        }

        cudaMemcpy(h_C, d_C, C_size, cudaMemcpyDeviceToHost);
        //int threads = gridHeight * gridWidth * BLOCK_SIZE * BLOCK_SIZE;
        //printf("Grid: %d x %d (threads = %d)\n", gridWidth, gridHeight, threads);

        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
}

extern "C"
{
    void matmult_gpu4(int m, int n, int k, double *h_A, double *h_B, double *h_C)
    {
        double t;
        int l;

        double *d_A, *d_B, *d_C;
        long A_size = m * k * sizeof(double);
        long B_size = k * n * sizeof(double);
        long C_size = m * n * sizeof(double);

        // Cuda Malloc
        cudaMalloc((void **)&d_A, A_size);
        cudaMalloc((void **)&d_B, B_size);
        cudaMalloc((void **)&d_C, C_size);

        // Transfer host data data to device
        cudaMemcpy(d_A, h_A, A_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B, B_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_C, h_C, C_size, cudaMemcpyHostToDevice);

        // Set gridsize
        int gridWidth = 1 + n / BLOCK_SIZE;
        int gridHeight = 1 + m / BLOCK_SIZE;

        // Halve the grid size horizontally or vertically, depending on which is true
        gridWidth = HORIZONTAL ? 1 + n / BLOCK_SIZE / REGISTER_SIZE : gridWidth;
        gridHeight = VERTICAL ? 1 + n / BLOCK_SIZE / REGISTER_SIZE : gridHeight;

        // printf("Grid: %d x %d", gridWidth, gridHeight);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 dimGrid(gridWidth, gridHeight, 1);

        t = omp_get_wtime();
        for (l = 0; l < REPETITIONS; l++)
        {
            kernel_v4<<<dimGrid, dimBlock>>>(m, n, k, d_A, d_B, d_C);
            cudaDeviceSynchronize();
        }
        // printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));

        cudaDeviceSynchronize();
        cudaMemcpy(h_C, d_C, C_size, cudaMemcpyDeviceToHost);

        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
}

extern "C"
{
    void matmult_gpu5(int m, int n, int k, double *h_A, double *h_B, double *h_C)
    {
        double t;
        int l;

        double *d_A, *d_B, *d_C;
        long A_size = m * k * sizeof(double);
        long B_size = k * n * sizeof(double);
        long C_size = m * n * sizeof(double);

        // Cuda Malloc
        cudaMalloc((void **)&d_A, A_size);
        cudaMalloc((void **)&d_B, B_size);
        cudaMalloc((void **)&d_C, C_size);

        // Transfer host data data to device
        cudaMemcpy(d_A, h_A, A_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B, B_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_C, h_C, C_size, cudaMemcpyHostToDevice);

        // Set gridsize
        int gridWidth = n / BLOCK_SIZE;
        int gridHeight = m / BLOCK_SIZE;

        // printf("Grid: %d x %d", gridWidth, gridHeight);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 dimGrid(gridWidth, gridHeight, 1);

        t = omp_get_wtime();
        for (l = 0; l < REPETITIONS; l++)
        {
            kernel_shared<<<dimGrid, dimBlock>>>(m, n, k, d_A, d_B, d_C);
            cudaDeviceSynchronize();
        }
        // printf("%f ", (omp_get_wtime() - t) / ((double)REPETITIONS));

        cudaMemcpy(h_C, d_C, C_size, cudaMemcpyDeviceToHost);
        //int threads = gridHeight * gridWidth * BLOCK_SIZE * BLOCK_SIZE;
        //printf("Grid: %d x %d (threads = %d)\n", gridWidth, gridHeight, threads);

        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
}
