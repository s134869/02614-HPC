#include "datatools.h"
#include <stdio.h>
#include <math.h>
#include <helper_cuda.h>
#include "cublas_v2.h"
#include <cuda_runtime.h>

/** Question 1: Sequential Jacobi */
extern "C"
{
#include <cblas.h>
    void jacobi_seq(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
    {
        int i, j;
        //int M = N + 2;
        double D = 0.25;
        double **tmp;
        double diff = INFINITY;
        int iteration = 0;
        double threshold_squared = threshold * threshold;

        while (diff > threshold_squared && iteration < max_iter)
        {
            diff = 0.0;
            tmp = U;
            U = U_old;
            U_old = tmp;

            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                                   U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                                   delta * delta * f[i][j]);

                    // Compute Frobenius norm
                    diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
                }
            }
            iteration++;
            // Print difference progress by itration
            //if (iteration % 500 == 0) {printf("%d %16g\n", iteration, diff); }
        }
        printf("%d %16.f", iteration, diff);
    }
}

/** Question 2: Sequential Gauss-Seidel */
extern "C"
{
#include <cblas.h>
    void gauss_seidel(int N, double **U, double **f, int max_iter, double threshold, double delta)
    {
        int i, j;
        //int M = N + 2;
        double D = 0.25;

        double tmp;
        double diff = INFINITY;
        int iteration = 0;
        double threshold_squared = threshold * threshold;

        while (diff > threshold_squared && iteration < max_iter)
        {
            diff = 0.0;

            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    tmp = D * (U[(i - 1)][j] + U[(i + 1)][j] +
                               U[(i)][j + 1] + U[(i)][j - 1] +
                               delta * delta * f[i][j]);

                    // Compute Frobenius norm
                    diff += (tmp - U[i][j]) * (tmp - U[i][j]);
                    U[i][j] = tmp;
                }
            }
            iteration++;
            // Print difference progress by itration
            //if (iteration % 500 == 0) {printf("%d %16g\n", iteration, diff); }
        }
        printf("%d %f ", iteration, diff);
    }
}

/** Question 3: OpenMP Jacobi (simple version) */
extern "C"
{
#include <cblas.h>
    void jacobi_omp_simple(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
    {
        int i, j;
        //int M = N + 2;
        double D = 0.25;
        double **tmp;
        double diff = INFINITY;
        int iteration = 0;
        double threshold_squared = threshold * threshold;

        while (diff > threshold_squared && iteration < max_iter)
        {
            diff = 0.0;
            tmp = U;
            U = U_old;
            U_old = tmp;
#pragma omp parallel for private(i, j) reduction(+ \
                                                 : diff)
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                                   U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                                   delta * delta * f[i][j]);

                    // Compute Frobenius norm
                    diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
                }
            }
            iteration++;
        }
        // printf("%d %f ", iteration, diff);
    }
}

/** OpenMP Jacobi (simple version with scheduling) */
extern "C"
{
#include <cblas.h>
    void jacobi_omp_simple_scheduling(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
    {
        int i, j;
        //int M = N + 2;
        double D = 0.25;
        double **tmp;
        double diff = INFINITY;
        int iteration = 0;
        double threshold_squared = threshold * threshold;

        while (diff > threshold_squared && iteration < max_iter)
        {
            diff = 0.0;
            tmp = U;
            U = U_old;
            U_old = tmp;
#pragma omp parallel for private(i, j) schedule(runtime) reduction(+ \
                                                                   : diff)
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                                   U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                                   delta * delta * f[i][j]);

                    // Compute Frobenius norm
                    diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
                }
            }
            iteration++;
        }
        printf("%d %f ", iteration, diff);
    }
}

/** Question 3: OpenMP Jacobi (with race condition) */
extern "C"
{
#include <cblas.h>
    void jacobi_omp_with_races(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
    {
        int i, j;
        //int M = N + 2;
        double D = 0.25;
        double **tmp;
        double diff = INFINITY;
        int iteration = 0;
        double threshold_squared = threshold * threshold;

        while (diff > threshold_squared && iteration < max_iter)
        {
            diff = 0.0;
            tmp = U;
            U = U_old;
            U_old = tmp;
#pragma omp parallel for shared(j) reduction(+ \
                                             : diff)
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                                   U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                                   delta * delta * f[i][j]);

                    // Compute Frobenius norm
                    diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
                }
            }
            iteration++;
        }
        printf("%d %f ", iteration, diff);
    }
}

extern "C"
{
#include <cblas.h>
    void jacobi_omp(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
    {
        int i, j;
        //int M = N + 2;
        double D = 0.25;
        double **tmp;
        double diff = INFINITY;
        double diff_tmp = INFINITY;
        int iteration = 0;
        double threshold_squared = threshold * threshold;

#pragma omp parallel private(i, j) firstprivate(iteration) reduction(+ \
                                                                     : diff)
        {
            while (diff_tmp > threshold_squared && iteration < max_iter)
            {
                diff = 0.0;

#pragma omp for schedule(static) private(i, j)
                for (i = 1; i <= N; i++)
                {
                    for (j = 1; j <= N; j++)
                    {
                        U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                                       U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                                       delta * delta * f[i][j]);

                        // Compute Frobenius norm
                        diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
                    }
                } // For barrier
                iteration++;

#pragma omp single
                {
                    diff_tmp = diff;
                    tmp = U;
                    U = U_old;
                    U_old = tmp;
                }
                //printf("Iteration %d for thread %d", iteration, omp_get_thread_num());
            }
            //#pragma omp single
            //{
            //printf("%d %f ", iteration, diff);
            //}

        } // End parallel
    }
}

__global__ void jacobi_omp_single_kernel(int N, double *U_old, double *U, double *f, double delta)
{
    int i, j;
    double D = 0.25;
    int M = N + 2;

    for (i = 1; i <= N; i++)
    {
        for (j = 1; j <= N; j++)
        {
            U[i * M + j] = D * (U_old[(i - 1) * M + j] +
                                U_old[(i + 1) * M + j] +
                                U_old[(i * M) + (j + 1)] +
                                U_old[(i * M) + (j - 1)] +
                                delta * delta * f[i * M + j]);
        }
    }
}

__global__ void jacobi_omp_multi_kernel(int N, double *U_old, double *U, double *f, double delta)
{
    int M = N + 2;
    int j = 1 + blockIdx.x * blockDim.x + threadIdx.x;
    int i = 1 + blockIdx.y * blockDim.y + threadIdx.y;

    if (i <= N && j <= N)
    {
        U[i * M + j] = 0.25 * (U_old[(i - 1) * M + j] +
                            U_old[(i + 1) * M + j] +
                            U_old[(i * M) + (j + 1)] +
                            U_old[(i * M) + (j - 1)] +
                            delta * delta * f[i * M + j]);
    }
}

// Kernel for running on multiple GPUs
__global__ void jacobi_omp_multi_gpu_kernel0(int N, double *U0_old, double *U1_old, double *U, double *f, double delta)
{
    int M = N + 2;
    int j = 1 + blockIdx.x * blockDim.x + threadIdx.x;
    int i = 1 + blockIdx.y * blockDim.y + threadIdx.y;
    
    // Until last line of U0
    if (i < (N / 2) && j <= N)
    {
        U[i * M + j] = 0.25 * (U0_old[(i - 1) * M + j] +
                            U0_old[(i + 1) * M + j] +
                            U0_old[(i * M) + (j + 1)] +
                            U0_old[(i * M) + (j - 1)] +
                            delta * delta * f[i * M + j]);
    }
    // Last line of U0, borrow from U1
    else if (i == (N / 2) && j <= N)
    {
        U[i * M + j] = 0.25 * (U0_old[(i - 1) * M + j] +   // Up
                            U1_old[j] +                 // Down - Borrow from first line in U1_old
                            U0_old[(i * M) + (j + 1)] + // Right
                            U0_old[(i * M) + (j - 1)] + // Left
                            delta * delta * f[i * M + j]);
    }
}

// Kernel for running on multiple GPUs
__global__ void jacobi_omp_multi_gpu_kernel1(int N, double *U0_old, double *U1_old, double *U, double *f, double delta)
{
    int M = N + 2;
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = 1 + blockIdx.y * blockDim.y + threadIdx.y;

    //First line of U1, borrow from U0
    if (i == 0 && j <= N)
    {
        U[i * M + j] = 0.25 * (U0_old[(N / 2) * M + j] +   // Up - Borrow from last line in U0_old
                            U1_old[(i + 1) * M + j] +   // Down
                            U1_old[(i * M) + (j + 1)] + // Right
                            U1_old[(i * M) + (j - 1)] + // Left
                            delta * delta * f[i * M + j]);
    }
    // Everything else than first line of U1
    else if (0 < i && i < (N / 2) && j <= N)
    {
        U[i * M + j] = 0.25 * (U1_old[(i - 1) * M + j] +
                            U1_old[(i + 1) * M + j] +
                            U1_old[(i * M) + (j + 1)] +
                            U1_old[(i * M) + (j - 1)] +
                            delta * delta * f[i * M + j]);
    }
}
