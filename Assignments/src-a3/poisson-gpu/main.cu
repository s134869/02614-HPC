#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "datatools.h"
#include "solvers.h"
#include <math.h>
#include <time.h>
#include <helper_cuda.h>
#include "cublas_v2.h"

#define BLOCK_SIZE 8
#define PRINT 0

int main(int argc, char *argv[])
{
    double wall_time;
    double start_time;

    int type = atoi(argv[1]);
    int N = atoi(argv[2]);
    int max_iter = atoi(argv[3]);
    double delta = 2.0 / ((double)N + 1.0);
    int M = N + 2;
    double memory_footprint = 3.0 * 8.0 * (double)M * (double)M / 1024.0;
    printf("%f,", memory_footprint);
    // Array size
    int matrixSize = M * M;
    long matrixBytes = matrixSize * sizeof(double);
    double *h_U_old, *d_U_old, *d0_U_old, *d1_U_old;
    double *h_U, *d_U, *d0_U, *d1_U;
    double *h_f, *d_f, *d0_f, *d1_f;

    int gridWidth = 1 + M / BLOCK_SIZE;
    dim3 dimGrid(gridWidth, gridWidth);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

    cudaMallocHost((void **)&h_U_old, matrixBytes);
    cudaMallocHost((void **)&h_U, matrixBytes);
    cudaMallocHost((void **)&h_f, matrixBytes);

    // Init host data
    init_jacobi(N, matrixSize, h_U, h_U_old, h_f);
    // if (PRINT)
    // {
    //     printf("init:\n");
    //     print_vec(M, h_U);

    //     printf("\nf:\n");
    //     print_vec(M, h_f);
    // }

    // Allocate memory device
    cudaMalloc((void **)&d_U_old, matrixBytes);
    cudaMalloc((void **)&d_U, matrixBytes);
    cudaMalloc((void **)&d_f, matrixBytes);

    // Copy from host to device
    cudaMemcpy(d_U_old, h_U_old, matrixBytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_U, h_U, matrixBytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_f, h_f, matrixBytes, cudaMemcpyHostToDevice);

    double *tmp;
    int iteration = 0;

    // Run sequential kernel implemetation
    if (type == 1)
    {
        start_time = omp_get_wtime();
        while (iteration < max_iter)
        {
            // Kernel launch
            jacobi_omp_single_kernel<<<1, 1>>>(N, d_U_old, d_U, d_f, delta);
            cudaDeviceSynchronize();

            iteration++;
            tmp = d_U;
            d_U = d_U_old;
            d_U_old = tmp;
        }
        wall_time = omp_get_wtime() - start_time;
        printf("%.3f\n", wall_time);

        // Run single thread per lattice point kernel
    }
    else if (type == 2)
    {
        start_time = omp_get_wtime();
        while (iteration < max_iter)
        {
            // Kernel launch
            jacobi_omp_multi_kernel<<<dimGrid, dimBlock>>>(N, d_U_old, d_U, d_f, delta);
            cudaDeviceSynchronize();

            iteration++;
            tmp = d_U;
            d_U = d_U_old;
            d_U_old = tmp;
        }
        wall_time = omp_get_wtime() - start_time;
        printf("%.3f\n", wall_time);

        // Run implementation using 2 GPUs
    }
    else if (type == 3)
    {
        int halfMatrixBytes = matrixBytes / 2;
        int halfMatrixSize = matrixSize / 2;
        double *tmp0, *tmp1;

        // Halve the grid size vertically
        int gridHeight = 1 + M / BLOCK_SIZE / 2;
        dim3 dimGrid(gridWidth, gridHeight);

        // DEVICE 0
        cudaSetDevice(0);
        //
        cudaMalloc((void **)&d0_U, halfMatrixBytes);
        cudaMalloc((void **)&d0_U_old, halfMatrixBytes);
        cudaMalloc((void **)&d0_f, halfMatrixBytes);
        //
        cudaMemcpy(d0_U, h_U, halfMatrixBytes, cudaMemcpyHostToDevice);
        cudaMemcpy(d0_U_old, h_U_old, halfMatrixBytes, cudaMemcpyHostToDevice);
        cudaMemcpy(d0_f, h_f, halfMatrixBytes, cudaMemcpyHostToDevice);

        // DEVICE 1
        cudaSetDevice(1);
        //
        cudaMalloc((void **)&d1_U, halfMatrixBytes);
        cudaMalloc((void **)&d1_U_old, halfMatrixBytes);
        cudaMalloc((void **)&d1_f, halfMatrixBytes);
        //
        cudaMemcpy(d1_U, h_U + halfMatrixSize, halfMatrixBytes, cudaMemcpyHostToDevice);
        cudaMemcpy(d1_U_old, h_U_old + halfMatrixSize, halfMatrixBytes, cudaMemcpyHostToDevice);
        cudaMemcpy(d1_f, h_f + halfMatrixSize, halfMatrixBytes, cudaMemcpyHostToDevice);

        cudaSetDevice(0);
        cudaDeviceEnablePeerAccess(1, 0);

        cudaSetDevice(1);
        cudaDeviceEnablePeerAccess(0, 0);

        start_time = omp_get_wtime();
        while (iteration < max_iter)
        {
            // Kernel launch on device 0
            cudaSetDevice(0);
            jacobi_omp_multi_gpu_kernel0<<<dimGrid, dimBlock>>>(N, d0_U_old, d1_U_old, d0_U, d0_f, delta);

            // Kernel launch on device 1
            cudaSetDevice(1);
            jacobi_omp_multi_gpu_kernel1<<<dimGrid, dimBlock>>>(N, d0_U_old, d1_U_old, d1_U, d1_f, delta);

            cudaDeviceSynchronize();
            // Swap 0
            tmp0 = d0_U;
            d0_U = d0_U_old;
            d0_U_old = tmp0;

            // Swap 1
            tmp1 = d1_U;
            d1_U = d1_U_old;
            d1_U_old = tmp1;

            iteration++;
        }
        wall_time = omp_get_wtime() - start_time;
        printf("%.3f\n", wall_time);

        // Memory transfer
        cudaSetDevice(0);
        cudaDeviceSynchronize();
        cudaMemcpy(h_U, d0_U, halfMatrixBytes, cudaMemcpyDeviceToHost);

        // Memory transfer
        cudaSetDevice(1);
        cudaDeviceSynchronize();
        cudaMemcpy(h_U + halfMatrixSize, d1_U, halfMatrixBytes, cudaMemcpyDeviceToHost);

        // Free memory device
        // cudaFree(d_U_old);
        // cudaFree(d_f);

        // Free memory host
        // cudaFreeHost(h_U_old);
        // cudaFreeHost(h_f);
    }

    // Copy result back from GPU
    //cudaMemcpy(h_U_old, d_U_old, matrixBytes, cudaMemcpyDeviceToHost);
    if (type < 3)
    {
        cudaMemcpy(h_U, d_U, matrixBytes, cudaMemcpyDeviceToHost);
    }

    //print_vec(M, h_U_old);
    if (PRINT)
    {
        printf("U final:\n");
        print_vec(M, h_U);
    }

    // Free memory device
    cudaFree(d_U_old);
    cudaFree(d_U);
    cudaFree(d_f);

    // Free memory host
    cudaFreeHost(h_U_old);
    cudaFreeHost(h_U);
    cudaFreeHost(h_f);

    return (0);
}
