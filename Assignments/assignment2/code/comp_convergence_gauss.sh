#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J gconvK
#BSUB -q hpcintro
#BSUB -r 
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 24

LOGEXT=$CC.dat

# Create results folder


if [ ! -d ../results/convergence ]; then
  mkdir -p  ../results/convergence;
fi

#poisson 2 100 1000000000 16 >> ../results/convergence/convergenceGauss100.$N$LOGEXT
#poisson 2 500 1000000000 16  >> ../results/convergence/convergenceGauss500.$N$LOGEXT
poisson 2 1000 1000000000 16 >> ../results/convergence/convergenceGauss1K.$N$LOGEXT

exit 0

