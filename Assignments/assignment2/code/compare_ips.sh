#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J ips
#BSUB -q hpcintro
#BSUB -R "rusage[mem=4GB]"
#BSUB -R "span[hosts=1]"

Narr="100 200 300 400 500 600 700 800 900 1000"

# Create results folder


if [ ! -d ../results/ips ]; then
  mkdir -p  ../results/ips;
fi

for N in $Narr
do
    poisson 1 $N 1000000000 16  >> ../results/ips/ipsJac_new.results.dat
    poisson 2 $N 1000000000 16  >> ../results/ips/ipsGaus_new.results.dat
    #OMP_NUM_THREADS=$num /bin/time -f "%e %U" -o  ./run_times/times$num$LOGEXT -a ./main_open $N
done
exit 0

