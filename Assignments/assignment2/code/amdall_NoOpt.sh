#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -o amNopt
#BSUB -J amNopt
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 24

THREADS="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"

# Create results folder

make -f Makefile.noOpt clean
make -f Makefile.noOpt

if [ ! -d ../results/speed_up ]; then
  mkdir -p  ../results/speed_up;
fi

for nThread in $THREADS
do
    OMP_NUM_THREADS=$nThread /bin/time poisson 3 400 20000 16  >> ../results/speed_up/400_3_noOpt.dat
    OMP_NUM_THREADS=$nThread /bin/time poisson 5 400 20000 16  >> ../results/speed_up/400_5_noOpt.dat
    OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=$nThread /bin/time poisson 6 400 20000 16  >> ../results/speed_up/400_6_noOpt.dat

    OMP_NUM_THREADS=$nThread /bin/time poisson 3 5000 1000 16  >> ../results/speed_up/5000_3_noOpt.dat
    OMP_NUM_THREADS=$nThread /bin/time poisson 5 5000 1000 16  >> ../results/speed_up/5000_5_noOpt.dat
    OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=$nThread /bin/time poisson 6 5000 1000 16  >> ../results/speed_up/5000_6_noOpt.dat
    #OMP_NUM_THREADS=$num /bin/time -f "%e %U" -o  ./run_times/times$num$LOGEXT -a ./main_open $N
done

## TIME INCREASE WITH THREADS AT BATCH SYSTEM BUT NOT IN TERMINAL
exit 0

