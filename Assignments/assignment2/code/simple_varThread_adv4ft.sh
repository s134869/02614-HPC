#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J adv4ft
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 24



if [ ! -d ../results/varSizeOMP ]; then
  mkdir -p  ../results/varSizeOMP;
fi

make clean
make

OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 1000 100000 8  >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 2000 34883 8   >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 3000 15789 8   >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 4000 8500 8    >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 5000 5000 8    >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 6000 4000 8    >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 7000 2727 8    >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 8000 2000 8    >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 9000 1500 8    >> ../results/varSizeOMP/adv_4ft.dat
OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=4 /bin/time poisson 6 10000 1000 8   >> ../results/varSizeOMP/adv_4ft.dat

#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 1000 100000 8  >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 2000 34883 8   >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 3000 15789 8   >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 4000 8500 8    >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 5000 5000 8    >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 6000 4000 8    >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 7000 2727 8    >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 8000 2000 8    >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 9000 1500 8    >> ../results/varSizeOMP/adv_4ft_cores.dat
#OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=4 /bin/time poisson 6 10000 1000 8   >> ../results/varSizeOMP/adv_4ft_cores.dat


exit 0