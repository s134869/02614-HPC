# Run the makefile using the proper analyzer flag "-xvpara" for detecting race conditions
make -f analyzer.make clean
make -f analyzer.make

# Run the Parallel Jacobi method, with race coniditions for a small system (30x30)
#OMP_WAIT_POLICY=active OMP_NUM_THREADS=4 collect -r on ./poisson_analyzer 9 30 10000 6
OMP_NUM_THREADS=4 collect -r on ./poisson_analyzer 9 30 10000 6

# Run the Parallel Jacobi method, with no race coniditions for a small system (30x30)
#OMP_WAIT_POLICY=active OMP_NUM_THREADS=4 collect -r on ./poisson_analyzer 3 30 10000 6
OMP_NUM_THREADS=4 collect -r on ./poisson_analyzer 3 30 10000 6