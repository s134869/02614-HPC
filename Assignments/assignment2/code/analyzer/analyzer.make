CC=suncc
DIR=../
SRCS	= $(DIR)main.c $(DIR)datatools.c $(DIR)solvers.c
OBJECTS	= $(SRCS:.c=.o)
NAME=poisson_analyzer
DEFS=-DOMP
CFLAGS=  $(DEFS) -g -fast -xopenmp -xloopinfo -xinstrument=datarace
LIBS = -lm

$(NAME): $(OBJECTS)
	 $(CC) $(OBJECTS) -o  $(NAME) $(CFLAGS) $(LIBS)

clean:
	@rm -f *.o core

main.o: $(DIR)main.c
