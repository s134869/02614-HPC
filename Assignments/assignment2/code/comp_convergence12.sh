#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J ips
#BSUB -q hpcintro
#BSUB -r 

Narr="100 500 1000"
LOGEXT=$CC.dat

# Create results folder


if [ ! -d ../results/ips ]; then
  mkdir -p  ../results/ips;
fi

for N in $Narr
do
    poisson 1 $N 1000000000 16 # >> ../results/ips/ips.results.$N$LOGEXT
    poisson 2 $N 1000000000 16 #>> ../results/ips/ips.results.$N$LOGEXT
    #OMP_NUM_THREADS=$num /bin/time -f "%e %U" -o  ./run_times/times$num$LOGEXT -a ./main_open $N
done
exit 0

