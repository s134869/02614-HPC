echo "#threads iter diff walltime SIMPLE"; 
for t in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16;
do echo -n " $t ";
	#OMP_NUM_THREADS=$t ./poisson 3 1000 10000 8; 
    OMP_PROC_BIND=true OMP_PLACES=cores OMP_NUM_THREADS=$t poisson 6 1000 50000 8
;
done

#echo "#threads iter diff walltime"; 
#for t in 1 2 4 8 16 24 32 48 60;
#do echo -n " $t ";
#	OMP_NUM_THREADS=$t ./poisson 3 1000 10000 8; 
#done