clear all
clc
close all

% N = 2000. Based on bash scipt runtimes_simpleomp_N400.sh

figure('Renderer', 'painters', 'Position', [10 10 1200 800])
simple = readtable('speed_up_simple_terminal.dat');
mandel = readtable('speed_up_simple_terminal.dat');
Nthreads = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16];

T_simple = simple{:,3};
S_simple = T_simple(1)./T_simple;

T_mandel = mandel{:,3};
S_mandel = T_mandel(1)./T_mandel;

%%
% Solve for f by T(2)
P = 16;
(1 - T(P)/T(1))/(1 - (1/P))

%%

plot(1:16 , 1:16, '.-' , 'markersize' , 15, 'linewidth' , 1.2)
hold on
plot(Nthreads , S , '.-' , 'markersize' , 15, 'linewidth' , 1.2)

xlim([1,16])
ylim([1,16])
grid on
hold off

xlabel('Processors' , 'FontSize' , 20, 'Interpreter' , 'LaTex')
ylabel('Speed-up', 'FontSize' , 20, 'Interpreter' , 'LaTex')
leg = legend({'$100 \%$ parallelization'},'Simple OMP')
set( leg  ,  'Fontsize'  ,   15 ,  'location'  ,  ' northwest ' , 'Interpreter' , 'latex')
set(gca,'FontSize',18)


