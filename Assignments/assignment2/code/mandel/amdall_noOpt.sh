#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -o speed_up_mandel
#BSUB -J speed_up_opt
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 24

THREADS="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"

# Create results folder



make -f Makefile.noOpt clean
make -f Makefile.noOpt


for nThread in $THREADS
do
    OMP_NUM_THREADS=$nThread /bin/time -f "%e %U" -o speed_upmandel_noOpt.dat -a mandelbrot
    #OMP_NUM_THREADS=$num /bin/time -f "%e %U" -o  ./run_times/times$num$LOGEXT -a ./main_open $N
done

## TIME INCREASE WITH THREADS AT BATCH SYSTEM BUT NOT IN TERMINAL
exit 0

