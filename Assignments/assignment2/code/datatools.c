#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>

// Using version fromm Bernd's datatools.c file
double **malloc_2d(int m, int n){
    int i;

    if (m <= 0 || n <= 0) return NULL;

    double **A = malloc(m * sizeof(double *));

    if (A == NULL) return NULL;

    A[0] = malloc(m*n*sizeof(double));
    
    if (A[0] == NULL) {
        free(A);
        return NULL;
    }

    for (i = 1; i < m; i++)
	    A[i] = A[0] + i * n;

    return A;
}

void free_2d(double **A) {
    free(A[0]);
    free(A);
}

void init_jacobi(int N, double *U, double *U_old, double *f) {
    int i,j;
    int M = N+2;
    double N_double = (double) N;

    // First the solution matrices
    // Initialize to 0
    for (i = 0; i < M*M; i++) {
            U[i] = 0;
            U_old[i] = 0;
            f[i] = 0;
    }
    
    // Boundaries
    // U(-1,y) = 20
    for (i = 0; i < M; i++) {
       U[i] = 20;
       U_old[i] = 20;
    }
    
    // U(x,1) = 20
    for (i = 0; i < M; i++) {
        U[M*(M-1) + i] = 20;
        U_old[M*(M-1) + i] = 20;
    }

    // U(x,-1) = 0
    for (i = 0; i < M; i++) {
        U[i * M] = 0;
        U_old[i * M] = 0;
       }

    // U(x,1) = 20
    for (i = 0; i < M; i++) {
        U[(M-1) + i*M] = 20;
        U_old[(M-1) + i*M] = 20;
    }

    // Source matrix
    // Set all to 0
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            f[i*M + j] = 0;
        }
    }

    // Set region to 200
    int i_lwb = (int)ceil((N_double + 1.0) / 2);
    int i_upb = (int)floor(2*(N_double + 1.0) / 3) + 1;

    int j_lwb = (int)ceil((N_double + 1.0) / 6);
    int j_upb = (int)floor((N_double + 1.0) / 3) + 1;

    for (i = i_lwb; i < i_upb; i++) {
        for (j = j_lwb; j < j_upb; j++) {
            f[i*M + j] = 200;
        }
    }
}


void init_jacobi_omp(int N, double *U, double *U_old, double *f) {
    int i,j;
    int M = N+2;
    double N_double = (double) N;

    // Parallel initialization
    // First the solution matrices
    #pragma omp parallel for schedule(static) shared(U,U_old,f,M) private(i) //schedule() // first touch // Initialize to 0
    for (i = 0; i < M*M; i++) {
            U[i] = 0;
            U_old[i] = 0;
            f[i] = 0;
    }
    
    // Boundaries
    // U(-1,y) = 20
    for (i = 0; i < M; i++) {
       U[i] = 20;
       U_old[i] = 20;
    }
    
    // U(x,1) = 20
    for (i = 0; i < M; i++) {
        U[M*(M-1) + i] = 20;
        U_old[M*(M-1) + i] = 20;
    }

    // U(x,-1) = 0
    for (i = 0; i < M; i++) {
        U[i * M] = 0;
        U_old[i * M] = 0;
    }

    // U(x,1) = 20
    for (i = 0; i < M; i++) {
        U[(M-1) + i*M] = 20;
        U_old[(M-1) + i*M] = 20;
    }

    // Source matrix
    // Set all to 0
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            f[i*M + j] = 0;
        }
    }

    // Set region to 200
    int i_lwb = (int) ceil((N_double + 1.0) / 2);
    int i_upb = (int) floor(2*(N_double + 1.0) / 3) + 1;

    int j_lwb = (int) ceil((N_double + 1.0) / 6);
    int j_upb = (int) floor((N_double + 1.0) / 3) + 1;

    for (i = i_lwb; i < i_upb; i++) {
        for (j = j_lwb; j < j_upb; j++) {
            f[i*M + j] = 200;
        }
    }
}

/**
 * Calculating Frobenius norm - not used as function is moved 
 */

double frob_norm_diff(double **A, double **B, int N) {
    int i,j;
    double sum = 0.0;
    double diff_element;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            diff_element = A[i][j] - B[i][j];

            sum += diff_element * diff_element;
        }
    }
    return(sqrt(sum));
}
    
void print_matrix(int N,double **M) {
    int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            printf("%1.f\t ", M[i][j]);
        }
        printf("\n");
    }
}


void print_matrix_to_file(int N,double **M , int type) {
    int i, j;
    char filename[100];
    
    if(type == 1){
        sprintf(filename, "../results/solution/solJac_N_%d.dat", N);
    }
    else if(type == 2){
        sprintf(filename, "../results/solution/solGaus_N_%d.dat", N);
    }
    else if(type == 3){
        printf("hej det her er N: %d" , N);
        sprintf(filename, "../results/solution/solSimpelOMPNyNy_N_%d.dat", N);
    }
    
    FILE *f = fopen(filename, "w");
    for (i = 0; i < (N+2); i++) {
        for (j = 0; j < (N+2); j++) {
            fprintf(f , "%1.f\t ", M[i][j]);
        }
        fprintf(f,"\n");
    }
}