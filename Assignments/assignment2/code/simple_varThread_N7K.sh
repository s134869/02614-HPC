#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J svT7K
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 16

Nthread="1 2 4 8 16"

#"100 200 400 "
# Create results folder


if [ ! -d ../results/varSizeOMP ]; then
  mkdir -p  ../results/varSizeOMP;
fi

for N in $Nthread
do
    OMP_NUM_THREADS=$N /bin/time poisson 3 7000 2727 16  >> ../results/varSizeOMP/simple_7000.dat
done
exit 0