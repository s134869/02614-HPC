#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -o mandel
#BSUB -J scale_mandel
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 24

THREADS="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"

# Create results folder


if [ ! -d ../results/speed_up ]; then
  mkdir -p  ../results/speed_up;
fi

# Reset results file
 > ../results/ips/speed_up_simple.dat

for nThread in $THREADS
do
    OMP_NUM_THREADS=$nThread /bin/time -o ../results/speed_up/mandel_scale.dat -f "%e %U" -a  ./mandel/mandelbrot
    #OMP_NUM_THREADS=$num /bin/time -f "%e %U" -o  ./run_times/times$num$LOGEXT -a ./main_open $N
done

## TIME INCREASE WITH THREADS AT BATCH SYSTEM BUT NOT IN TERMINAL
exit 0

