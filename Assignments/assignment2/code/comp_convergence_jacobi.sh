#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J jconK
#BSUB -q hpcintro
#BSUB -r 
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 24

LOGEXT=$CC.dat

# Create results folder


if [ ! -d ../results/convergence ]; then
  mkdir -p  ../results/convergence;
fi

#poisson 1 100 1000000000 16  >> ../results/convergence/convergenceJacobi100.$N$LOGEXT
#poisson 1 500 1000000000 16  >> ../results/convergence/convergenceJacobi500.$N$LOGEXT
poisson 1 1000 1000000000 16 >> ../results/convergence/convergenceJacobi1K.$N$LOGEXT

exit 0

