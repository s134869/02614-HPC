#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "datatools.h"
#include "solvers.h"
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
    int type = atoi(argv[1]);
    int N = atoi(argv[2]);
    int max_iter = atoi(argv[3]);
    int tresh = atoi(argv[4]);
    int i;

    double delta = 2.0 / ((double)N + 1.0);
    double threshold = 1 / pow(10, tresh);
    double M = N + 2;

    // Jacobi:
    double **U_old = malloc_2d(M, M);
    double **U = malloc_2d(M, M);
    double **f = malloc_2d(M, M);
    double wall_time;
    double start_time;

    // OMP_NUM_THREADS=16 /bin/time poisson 3 400 30000 6

    // If simple jacobi
    if (type == 1)
    {
        init_jacobi(N, *U, *U_old, *f);

        start_time = omp_get_wtime();
        jacobi_seq(N, U_old, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n" , wall_time);

        //print_matrix_to_file(N,U,1);
    }

    // If gauss_seidel
    else if (type == 2)
    {
        init_jacobi(N, *U, *U_old, *f);

        start_time = omp_get_wtime();
        gauss_seidel(N, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n" , wall_time);

        //print_matrix_to_file(N,U,2); // Print solution to file
    }

    // If simple parallel
    else if (type == 3)
    {
        init_jacobi(N, *U, *U_old, *f);

        start_time = omp_get_wtime();
        jacobi_omp_simple(N, U_old, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n" , wall_time);

        //print_matrix_to_file(N,U,3); // print solution to file
    }
    
    // If simple parallel w. scheduling
    else if (type == 4)
    {
        init_jacobi(N, *U, *U_old, *f);
        start_time = omp_get_wtime();
        jacobi_omp_simple_scheduling(N, U_old, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n", wall_time);
    }

    // If advanced parallel
    else if (type == 5)
    {
        init_jacobi(N, *U, *U_old, *f);
        start_time = omp_get_wtime();
        jacobi_omp(N, U_old, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n", wall_time);
    }

    // If advanced parallel with first touch and binding
    else if (type == 6)
    {
        init_jacobi_omp(N, *U, *U_old, *f);
        start_time = omp_get_wtime();
        jacobi_omp(N, U_old, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n", wall_time);
    }

    // Danger: Simple parallel with race conditions!
    // (Race conditions on purpose for Thread Analysis in Sun Studio)
    else if (type == 9)
    {
        init_jacobi(N, *U, *U_old, *f);
        start_time = omp_get_wtime();
        jacobi_omp_with_races(N, U_old, U, f, max_iter, threshold, delta);
        wall_time = omp_get_wtime() - start_time;
        printf(" %f\n", wall_time);
    }

    // Free memory
    free_2d(U);
    free_2d(U_old);
    free_2d(f);

    return (0);
}
