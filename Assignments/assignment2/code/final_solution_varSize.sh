#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 24:00
#BSUB -J final_sol
#BSUB -q hpcintro
#BSUB -R "rusage[mem=500MB]"
#BSUB -n 4
#BSUB -R "span[hosts=1]"

#"100 200 400 "
# Create results folder


if [ ! -d ../results/varSizeOMP ]; then
  mkdir -p  ../results/varSizeOMP;
fi

make clean
make

#poisson 1 50 2000000 16  #>> ../results/varSizeOMP/simple_10000.dat
#poisson 2 50 2000000 16  #>> ../results/varSizeOMP/simple_10000.dat
#OMP_NUM_THREADS=4 poisson 3 50 2000000 16  #>> ../results/varSizeOMP/simple_10000.dat

#poisson 1 100 2000000 8  #>> ../results/varSizeOMP/simple_10000.dat
#poisson 2 100 2000000 8  #>> ../results/varSizeOMP/simple_10000.dat
#OMP_NUM_THREADS=4 poisson 3 100 2000000 8  #>> ../results/varSizeOMP/simple_10000.dat

#poisson 1 500 2000000 4  #>> ../results/varSizeOMP/simple_10000.dat
#poisson 2 500 2000000 4  #>> ../results/varSizeOMP/simple_10000.dat
#OMP_NUM_THREADS=4 poisson 3 500 2000000 4  #>> ../results/varSizeOMP/simple_10000.dat

#poisson 1 1000 2000000 2  #>> ../results/varSizeOMP/simple_10000.dat
#poisson 2 1000 2000000 2  #>> ../results/varSizeOMP/simple_10000.dat
OMP_NUM_THREADS=4 poisson 3 1000 20000 4  #>> ../results/varSizeOMP/simple_10000.dat

# poisson 1 10000 2000000 4  #>> ../results/varSizeOMP/simple_10000.dat
# poisson 2 10000 2000000 4  #>> ../results/varSizeOMP/simple_10000.dat
# poisson 3 10000 2000000 4  #>> ../results/varSizeOMP/simple_10000.dat
exit 0