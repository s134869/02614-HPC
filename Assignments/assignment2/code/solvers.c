#include "datatools.h"
#include <stdio.h>
#include <math.h>

/** Question 1: Sequential Jacobi */
void jacobi_seq(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
{
    int i, j;
    int M = N + 2;
    double D = 0.25;
    double **tmp;
    double diff = INFINITY;
    int iteration = 0;
    double threshold_squared = threshold * threshold;

    while (diff > threshold_squared && iteration < max_iter)
    {
        diff = 0.0;
        tmp = U;
        U = U_old;
        U_old = tmp;

        for (i = 1; i <= N; i++)
        {
            for (j = 1; j <= N; j++)
            {
                U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                               U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                               delta * delta * f[i][j]);
                               
                // Compute Frobenius norm
                diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
            }
        }
        iteration++;
        // Print difference progress by itration
        //if (iteration % 500 == 0) {printf("%d %16g\n", iteration, diff); }
    }
    printf("%d %16.f", iteration, diff);
}

/** Question 2: Sequential Gauss-Seidel */
void gauss_seidel(int N, double **U, double **f, int max_iter, double threshold, double delta)
{
    int i, j;
    int M = N + 2;
    double D = 0.25;

    double tmp;
    double diff = INFINITY;
    int iteration = 0;
    double threshold_squared = threshold * threshold;

    while (diff > threshold_squared && iteration < max_iter)
    {
        diff = 0.0;

        for (i = 1; i <= N; i++)
        {
            for (j = 1; j <= N; j++)
            {
                tmp = D * (U[(i - 1)][j] + U[(i + 1)][j] +
                           U[(i)][j + 1] + U[(i)][j - 1] +
                           delta * delta * f[i][j]);

                // Compute Frobenius norm
                diff += (tmp - U[i][j]) * (tmp - U[i][j]);
                U[i][j] = tmp;
            }
        }
        iteration++;
        // Print difference progress by itration
        //if (iteration % 500 == 0) {printf("%d %16g\n", iteration, diff); }
    }
    printf("%d %f ", iteration, diff);
}

/** Question 3: OpenMP Jacobi (simple version) */
void jacobi_omp_simple(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
{
    int i, j;
    int M = N + 2;
    double D = 0.25;
    double **tmp;
    double diff = INFINITY;
    int iteration = 0;
    double threshold_squared = threshold * threshold;

    while (diff > threshold_squared && iteration < max_iter)
    {
        diff = 0.0;
        tmp = U;
        U = U_old;
        U_old = tmp;
        #pragma omp parallel for private(i, j) reduction(+: diff)
        for (i = 1; i <= N; i++)
        {
            for (j = 1; j <= N; j++)
            {
                U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                               U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                               delta * delta * f[i][j]);

                // Compute Frobenius norm
                diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
            }
        }
        iteration++;
    }
    // printf("%d %f ", iteration, diff);
}

/** OpenMP Jacobi (simple version with scheduling) */
void jacobi_omp_simple_scheduling(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
{
    int i, j;
    int M = N + 2;
    double D = 0.25;
    double **tmp;
    double diff = INFINITY;
    int iteration = 0;
    double threshold_squared = threshold * threshold;

    while (diff > threshold_squared && iteration < max_iter)
    {
        diff = 0.0;
        tmp = U;
        U = U_old;
        U_old = tmp;
        #pragma omp parallel for private(i, j) schedule(runtime) reduction(+: diff)
        for (i = 1; i <= N; i++)
        {
            for (j = 1; j <= N; j++)
            {
                U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                               U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                               delta * delta * f[i][j]);

                // Compute Frobenius norm
                diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
            }
        }
        iteration++;
    }
    printf("%d %f ", iteration, diff);
}

/** Question 3: OpenMP Jacobi (with race condition) */
void jacobi_omp_with_races(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
{
    int i, j;
    int M = N + 2;
    double D = 0.25;
    double **tmp;
    double diff = INFINITY;
    int iteration = 0;
    double threshold_squared = threshold * threshold;

    while (diff > threshold_squared && iteration < max_iter)
    {
        diff = 0.0;
        tmp = U;
        U = U_old;
        U_old = tmp;
        #pragma omp parallel for shared(j) reduction(+: diff)
        for (i = 1; i <= N; i++)
        {
            for (j = 1; j <= N; j++)
            {
                U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                               U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                               delta * delta * f[i][j]);

                // Compute Frobenius norm
                diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
            }
        }
        iteration++;
    }
    printf("%d %f ", iteration, diff);
}

void jacobi_omp(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta)
{
    int i, j;
    int M = N + 2;
    double D = 0.25;
    double **tmp;
    double diff = INFINITY;
    double diff_tmp = INFINITY;
    int iteration = 0;
    double threshold_squared = threshold * threshold;

    #pragma omp parallel private(i, j) firstprivate(iteration) reduction(+: diff)
    {
        while (diff_tmp > threshold_squared && iteration < max_iter){
            diff = 0.0;
            
            #pragma omp for  schedule(static) private(i, j)
            for (i = 1; i <= N; i++){
                for (j = 1; j <= N; j++)
                {
                    U[i][j] = D * (U_old[(i - 1)][j] + U_old[(i + 1)][j] +
                                   U_old[(i)][j + 1] + U_old[(i)][j - 1] +
                                   delta * delta * f[i][j]);

                    // Compute Frobenius norm
                    diff += (U[i][j] - U_old[i][j]) * (U[i][j] - U_old[i][j]);
                }
            } // For barrier
            iteration++;
        
            #pragma omp single
            {
                diff_tmp = diff;
                tmp = U;
                U = U_old;
                U_old = tmp;
            }
            //printf("Iteration %d for thread %d", iteration, omp_get_thread_num());
        }
        //#pragma omp single
        //{
        //printf("%d %f ", iteration, diff);
        //}
        
    } // End parallel
}
