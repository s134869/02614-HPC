#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J adv16
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 16



if [ ! -d ../results/varSizeOMP ]; then
  mkdir -p  ../results/varSizeOMP;
fi

make clean
make

OMP_NUM_THREADS=16 /bin/time poisson 5 1000 100000 8  >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 2000 34883 8   >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 3000 15789 8   >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 4000 8500 8    >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 5000 5000 8    >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 6000 4000 8    >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 7000 2727 8    >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 8000 2000 8    >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 9000 1500 8    >> ../results/varSizeOMP/adv_16.dat
OMP_NUM_THREADS=16 /bin/time poisson 5 10000 1000 8   >> ../results/varSizeOMP/adv_16.dat


exit 0