#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J lib_non_opt

LOGEXT=$CC.dat

# Create results folder



if [ ! -d ./block_comp ]; then
  mkdir -p  ./block_comp;
fi

let n=2000+20
let k=n+50

for blk in $block
  do
    ./matmult_c.gcc lib 1000 $k $n | grep -v CPU >> ./block_comp/lib_non_opt$LOGEXT
  done

exit 0

