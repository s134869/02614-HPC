#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J Permutation_job_small

M="10 50 100 200 500 750 1000"
permute="mnk mkn nmk nkm kmn knm"
LOGEXT=$CC.dat


# Create results folder



if [ ! -d ./permutation_results_small ]; then
  mkdir -p  ./permutation_results_small;
fi

for perm in $permute    
do
    for m in $M
    do
        let n=m+20
        let k=n+50
        ./matmult_c.gcc $perm $m $k $n | grep -v CPU >> ./permutation_results_small/$perm$LOGEXT
    done
done    

exit 0

