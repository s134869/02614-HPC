#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 1
#BSUB -J blk_perm_opt
#BSUB -o blk_perm_opt.out
#BSUB -q hpcintro

# NOTE: Change the name every time a new combination is tested!
LOGEXT=blk_permutation_knm_opt.dat
M="10 50 100 200 500 750 1000"
dir=results
name=blk_permutations

# Create folder if not exist
if [ ! -d ./$dir/$name/]; then
  mkdir -p ./$dir/$name/;
fi

for m in $M
	do
  		let n=m+20
		let k=n+50
		./matmult_c.gcc blk $m $k $n 70 | grep -v CPU >> ./$dir/$name/$LOGEXT
	done

exit 0
