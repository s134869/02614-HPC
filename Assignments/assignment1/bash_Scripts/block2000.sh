#!/bin/sh
#BSUB -B
#BSUB -N
#BSUB -W 10:00
#BSUB -J Block_test2000

block="2 5 10 20 30 60 70 90 120 150 200 500 1000 2000"
LOGEXT=$CC.dat

# Create results folder



if [ ! -d ./block_comp ]; then
  mkdir -p  ./block_comp;
fi

let n=2000+20
let k=n+50

for blk in $block
  do
    ./matmult_c.gcc blk 1000 $k $n $blk | grep -v CPU >> ./block_comp/2000$LOGEXT
  done

exit 0

