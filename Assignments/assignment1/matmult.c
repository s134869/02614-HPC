//
// Created by Thomas Nygaard Nilsson on 2019-01-09.
//
#include <cblas.h>
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void matmult_nat(int m, int n, int k, double **A, double **B, double **C) {
    // Input:
    //      Integers m, n, k (matrix dimensions)
    //      Matrix A (m x k)
    //      Matrix B (k x n)
    // Output:
    //      Matrix C: m x n
    int i, j, l;
    double sum;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            sum = 0;
            for (l = 0; l < k; l++) {
                sum += A[i][l] * B[l][j];
            }
            C[i][j] = sum;
        }
    }
}

void matmult_lib(int m, int n, int k, double **A, double **B, double **C) {
    // Input:
    //      Integers m, n, k (matrix dimensions)
    //      Matrix A (m x k)
    //      Matrix B (k x n)
    // Output:
    //      Matrix C: m x n
    cblas_dgemm(CblasRowMajor , CblasNoTrans, CblasNoTrans ,  m , n , k , 1.0, *A,  k , *B , n , 0.0 , *C , n);
}


void matmult_mnk(int m, int n, int k, double **A, double **B, double **C) {
    // Input:
    //      Integers m, n, k (matrix dimensions)
    //      Matrix A (m x k)
    //      Matrix B (k x n)
    // Output:
    //      Matrix C: m x n
    int i, j, l;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            for (l = 0; l < k; l++) {
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_mkn(int m, int n, int k, double **A, double **B, double **C) {
    int i, j, l;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (i = 0; i < m; i++) {
        for (l = 0; l < k; l++) {
            for (j = 0; j < n; j++) {
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_nmk(int m, int n, int k, double **A, double **B, double **C) {
    int i, j, l;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            for (l = 0; l < k; l++) {
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_nkm(int m, int n, int k, double **A, double **B, double **C) {
    int i, j, l;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (j = 0; j < n; j++) {
        for (l = 0; l < k; l++) {
            for (i = 0; i < m; i++) {
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_kmn(int m, int n, int k, double **A, double **B, double **C) {
    int i, j, l;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (l = 0; l < k; l++) {
        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++) {
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_knm(int m, int n, int k, double **A, double **B, double **C) {
    int i, j, l;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (l = 0; l < k; l++) {
        for (j = 0; j < n; j++) {
            for (i = 0; i < m; i++) {
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_blk(int m, int n, int k, double **A, double **B, double **C , int blk) {
    int i, j, l;
    int i2, j2 , l2;

    // Set C to zero
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            C[i][j] = 0;

    for (i = 0 ; i < m ; i+=blk){
        for(l = 0 ; l < k ; l += blk){
            for (j = 0 ; j < n ; j += blk){
                for(i2 = i ; i2 < MIN(m,i+blk) ; i2++){
                    for(l2 = l ; l2 < MIN(k,l+blk) ; l2++){
                        for(j2 = j ; j2 < MIN(n,j+blk) ; j2++){
                            C[i2][j2] += A[i2][l2] * B[l2][j2];
                        }
                    }
                }
            }
        }
    }
}