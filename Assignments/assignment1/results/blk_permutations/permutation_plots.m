%%
clear all
clc
clf
configs = {'kmn', 'knm', 'mkn', 'mnk', 'nkm', 'nmk'};
matrix = [10 50 100 200 500 750 1000];
figure(1)
hold all

for i = 1:length(configs) 
    filen = sprintf('blk_permutation_%s.dat', configs{i})
    perm_results = readtable(filen);
    plot(matrix, perm_results{:,2}, '.-', 'markersize', 15, 'linewidth', 1.2)
end

hold off
grid on
xlabel('Matrix sizes' , 'FontSize' , 12);
ylabel('Performance [MFLOP/s]', 'FontSize' , 12);
leg = legend('kmn','knm','mkn','mnk','nkm','nmk');
set(leg,  'Fontsize',  8,  'location',  'northeastoutside');
saveas(gcf, '02614-HPC/Assignments/assignment1/results/blk_permutations/permutation_comp2', 'epsc');
