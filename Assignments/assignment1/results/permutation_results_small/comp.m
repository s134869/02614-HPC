clc
clf
[kmn.mem , kmn.fl , stat , kmn.mod] = textread('kmn.dat' , '%f %f %d %s' , 'delimiter',  '\n');
[knm.mem , knm.fl , stat , mod] = textread('knm.dat' , '%f %f %d %s' , 'delimiter',  '\n');
[mkn.mem , mkn.fl , stat , mod] = textread('mkn.dat' , '%f %f %d %s' , 'delimiter',  '\n');
[mnk.mem , mnk.fl , stat , mod] = textread('mnk.dat' , '%f %f %d %s' , 'delimiter',  '\n');
[nkm.mem , nkm.fl , stat , mod] = textread('nkm.dat' , '%f %f %d %s' , 'delimiter',  '\n');
[nmk.mem , nmk.fl , stat , mod] = textread('nmk.dat' , '%f %f %d %s' , 'delimiter',  '\n');

figure(1)
hold all
plot(kmn.mem , kmn.fl , '.-' , 'markersize' , 15, 'linewidth' , 1.2)
plot(knm.mem , knm.fl  , '.-' , 'markersize' , 15,'linewidth' , 1.2)
plot(mkn.mem , mkn.fl  , '.-' , 'markersize' , 15, 'linewidth' , 1.2')
plot(mnk.mem , mnk.fl  ,'.-' , 'markersize' , 15, 'linewidth' , 1.2)
plot(nkm.mem , nkm.fl  , '.-' , 'markersize' , 15, 'linewidth' , 1.2)
plot(nmk.mem , nmk.fl  , '.-' , 'markersize' ,15, 'linewidth' , 1.2)
hold off
grid on
xlabel('Memory footprint [kB]' , 'FontSize' , 12)
ylabel('Performance [MFLOP/s]', 'FontSize' , 12)
leg = legend('kmn','knm','mkn','mnk','nkm','nmk')
set( leg  ,  'Fontsize'  ,  8  ,  'location'  ,  ' northeast ' )
saveas(gcf,'test_plot','epsc')
