#!/bin/sh
#BSUB -W 10:00
#BSUB -J gpu4_v8
#BSUB -r 
#BSUB -R "rusage[mem=6GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpuh8.txt

module load cuda/10.0
module load gcc/7.3.0

N="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"

output1=data/gpu4_v8.dat

# Make header and flush file contents
echo "memory,mflops">$output1


if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu4 $n $n $n | awk '{ print $1","$2","$3}'>>$output1
done

exit 0

