#!/bin/sh
#BSUB -W 00:15
#BSUB -J poissomp
#BSUB -r 
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 16
#BSUB -o data/jobs/poiss_omp.txt

module load cuda/10.0
module load gcc/7.3.0

S="50 100 200 300 400 500 600 700 800 900 1000"
L="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"
output1=data/poisson_omp_S.dat
output2=data/poisson_omp_L.dat

# Make header and flush file contents
echo "memory,time">$output1
echo "memory,time">$output2

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $S
do
    echo "Running for n = $n";
    OMP_NUM_THREADS=16 ./poisson-omp/poisson 6 $n 1000 1 | awk '{print $1}'>>$output1
done

for n in $L
do
    echo "Running for n = $n";
    OMP_NUM_THREADS=16 ./poisson-omp/poisson 6 $n 1000 1 | awk '{print $1}'>>$output2
done

exit 0

