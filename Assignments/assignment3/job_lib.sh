#!/bin/sh
#BSUB -W 00:30
#BSUB -J gpulib
#BSUB -r 
#BSUB -R "rusage[mem=5GB]"
#BSUB -R "span[hosts=1]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu4.txt
#BSUB -n 24

module load cuda/10.0
module load gcc/7.3.0

S="10 50 100 200 300 400 500 600 700 800 900 1000"
L="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"

output1=data/lib_S_1thread.dat
output2=data/lib_S_6thread.dat
output3=data/lib_S_12thread.dat
output4=data/lib_S_24thread.dat

output5=data/lib_L_1thread.dat
output6=data/lib_L_6thread.dat
output7=data/lib_L_12thread.dat
output8=data/lib_L_24thread.dat


# Make header and flush file contents
echo "memory,mflops">$output1
echo "memory,mflops">$output2
echo "memory,mflops">$output3
echo "memory,mflops">$output4

echo "memory,mflops">$output5
echo "memory,mflops">$output6
echo "memory,mflops">$output7
echo "memory,mflops">$output8

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $S
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=1 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output1
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=6 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output2
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=12 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output3
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=24 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output4
done

for n in $L
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=1 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output5
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=6 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output6
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=12 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output7
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 OMP_NUM_THREADS=24 numactl --cpunodebind=0 ./matmult_f.nvcc lib $n $n $n | awk '{ print $1","$2","$3}'>>$output8
done

exit 0

