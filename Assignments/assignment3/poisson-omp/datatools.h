void init_jacobi(int N , double *U, double *U_old , double *f);
double frob_norm_diff(double **A , double **B , int N);
void print_matrix(int N,double **M);
double **malloc_2d(int m, int n);
void free_2d(double **A);
void init_jacobi_omp(int N , double *U, double *U_old , double *f);
void print_matrix_to_file(int N,double **M , int type);
