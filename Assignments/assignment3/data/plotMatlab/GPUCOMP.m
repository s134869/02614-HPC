clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;


gpu2_32 = importdata('../gpu2_32.dat',delimiterIn,headerlinesIn);
gpu2_32 = gpu2_32.data;

gpu3_v_16 = importdata('../gpu3_v_16.dat',delimiterIn,headerlinesIn);
gpu3_v_16 = gpu3_v_16.data;

gpu4_h4 = importdata('../gpu4_h4.dat',delimiterIn,headerlinesIn);
gpu4_h4 = gpu4_h4.data;

gpu5_32 = importdata('../gpu5_32.dat',delimiterIn,headerlinesIn);
gpu5_32 = gpu5_32.data;

gpulib_32 = importdata('../gpulib_32.dat',delimiterIn,headerlinesIn);
gpulib_32 = gpulib_32.data;

% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000];
mem = 2*N.^3';

Gfs2_32 = mem ./ gpu2_32(:,1) / 10^12;
Gfs_3_v_16 = mem./ gpu3_v_16(:,1) / 10^12;

Gfs5_32 = mem ./ gpu5_32(:,1) / 10^12;
Gfs4_h4 = mem./ gpu4_h4(:,1) / 10^12;

Gfs_gpulib = mem ./ gpulib_32(:,1) / 10^12;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(gpu5_32(:,2) , Gfs5_32 ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(4,:) )
hold on
semilogx(gpu4_h4(:,2) , Gfs4_h4 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(4,:), 'linewidth', 1.2, 'color',colours(4,:) )
semilogx(gpu2_32(:,2) , Gfs2_32 ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(1,:) )
semilogx(gpu2_32(:,2) , Gfs_3_v_16 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:)); 
semilogx(gpulib_32(:,2) , Gfs_gpulib ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(5,:) )
hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('TFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('H8' , 'V8' , 'H16' , 'V16' , 'H32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpu3','epsc')

%%
plot(Nthreads , T_mandel , '-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:));
plot(Nthreads , T_3noOpt ,  '-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(2,:));

