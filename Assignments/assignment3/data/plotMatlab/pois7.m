clear all
clc
close all 

IT = 1000;

delimiterIn = ',';
headerlinesIn = 1;

omp = importdata('../poisson_omp_S.dat',delimiterIn,headerlinesIn);
omp = omp.data;

gpu16 = importdata('../poisson1_16.dat',delimiterIn,headerlinesIn);
gpu16 = gpu16.data;

gpu32 = importdata('../poisson1_32.dat',delimiterIn,headerlinesIn);
gpu32 = gpu32.data;



% Compute Gflops
N = [50 100 200 300 400 500 600 700 800 900 1000]';
flop = 11*N.^2 * IT;


Gfs_omp = flop ./ omp(:,2) / 10^9;
Gfs_gpu16 = flop./ gpu16(:,2) / 10^9;
Gfs_gpu32 = flop./ gpu32(:,2) / 10^9;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
loglog(omp(:,1) , Gfs_omp ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
loglog(gpu16(:,1) , Gfs_gpu16   ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
loglog(gpu32(:,1) , Gfs_gpu32 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(3,:), 'linewidth', 1.2, 'color',colours(3,:) )
hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('GFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('Open MP' , 'Sequential GPU Block size 16' , 'Sequential GPU Block size 32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' best ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'pois7','epsc')

