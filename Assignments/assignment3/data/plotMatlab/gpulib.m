clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;

gpu_8 = importdata('../gpulib_8.dat',delimiterIn,headerlinesIn);
gpu_8 = gpu_8.data;

gpu_16 = importdata('../gpulib_16.dat',delimiterIn,headerlinesIn);
gpu_16 = gpu_16.data;

gpu_32 = importdata('../gpulib_32.dat',delimiterIn,headerlinesIn);
gpu_32 = gpu_32.data;


% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 11000 12000 13000 14000 15000 16000 17000 18000 19000 2000]';
mem = 2*N.^3;

Gfs_8 = mem ./   gpu_8(:,1) / 10^9;
Gfs_16 = mem./ gpu_16(:,1) / 10^9 ;

Gfs_32 = mem ./ gpu_32(:,1)  / 10^9 ;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(gpu_8(:,2) , gpu_8(:,3)/10^6 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
semilogx(gpu_16(:,2) , gpu_16(:,3)/10^6 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
semilogx(gpu_32(:,2) , gpu_32(:,3)/10^6 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('TFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('Block size 8' , 'Block size 16' , 'Block size 32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpulib','epsc')

%% CUBLAS COMP TO OTHERS
N_other = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000]';
mem_N = 2*N_other.^3;


gpu_32_2 = importdata('../gpu2_32.dat',delimiterIn,headerlinesIn);
gpu_32_2 = gpu_32_2.data;
Gfs_32_2 = mem_N ./ gpu_32_2(:,1) / 10^9;

gpu_16v = importdata('../gpu3_v_16.dat',delimiterIn,headerlinesIn);
gpu_16v = gpu_16v.data;
Gfs_16v = mem_N ./ gpu_16v(1:10,1) / 10^9;

gpu_h4 = importdata('../gpu4_h4.dat',delimiterIn,headerlinesIn);
gpu_h4 = gpu_h4.data;
Gfs_4h = mem_N ./ gpu_h4(:,2) / 10^9;

gpu5_32 = importdata('../gpu5_32.dat',delimiterIn,headerlinesIn);
gpu5_32 = gpu5_32.data;
Gfs5_32 = mem_N ./ gpu5_32(1:10,1) / 10^12;

gpu_lib = importdata('../gpulib_32.dat',delimiterIn,headerlinesIn);
gpu_lib = gpu_lib.data;
Gfs_32_lib = mem(1:10) ./ gpu_lib(1:10,1)  / 10^9 ;



% Compute Gflops
N = [960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480];
mem5 = 2*N.^3';

Gfs5_32 = mem5 ./ gpu5_32(:,1) / 10^9;

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(gpu_32_2(:,2) , Gfs_32_2 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
semilogx(gpu_16v(:,2) , Gfs_16v ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
semilogx(gpu_h4(:,1) , gpu_h4(:,2)/10^3 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(3,:), 'linewidth', 1.2, 'color',colours(3,:) )
semilogx(gpu5_32(:,2) , Gfs5_32 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(6,:), 'linewidth', 1.2, 'color',colours(6,:) )
semilogx(gpu_lib(:,2) , Gfs_32_lib ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('GFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('gpu2' , 'best gpu3' , 'best gpu4' , 'gpu5' , 'gpulib' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpulib_comp','epsc')
