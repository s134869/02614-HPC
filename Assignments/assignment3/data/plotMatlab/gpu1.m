clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;

gpu_1 = importdata('../gpu1_small.dat',delimiterIn,headerlinesIn);
gpu_1 = gpu_1.data;



gpu_lib1 = importdata('../lib_S_1thread.dat',delimiterIn,headerlinesIn);
gpu_lib1 = gpu_lib1.data;

gpu_lib6 = importdata('../lib_S_6thread.dat',delimiterIn,headerlinesIn);
gpu_lib6 = gpu_lib6.data;

gpu_lib12 = importdata('../lib_S_12thread.dat',delimiterIn,headerlinesIn);
gpu_lib12 = gpu_lib12.data;

gpu_lib24 = importdata('../lib_S_24thread.dat',delimiterIn,headerlinesIn);
gpu_lib24 = gpu_lib24.data;

% Compute Gflops
N1 = [10 50 100 200 300 400 500 600 700 800];
N = [10 50 100 200 300 400 500 600 700 800 900 1000];

flops1 = 2*N1.^3';
flops = 2*N.^3';

gpu1 = flops1./ gpu_1(:,1) / 10^9;
lib1 = flops ./ gpu_lib1(:,1) / 10^9;
lib6 = flops ./ gpu_lib6(:,1) / 10^9;
lib12 = flops ./ gpu_lib12(:,1) / 10^9;
lib24 = flops ./ gpu_lib24(:,1) / 10^9;



colours = permute(get(gca, 'colororder'), [1 3 2]);


%%
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
loglog(gpu_1(:,2) , gpu1 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours( 5,:), 'linewidth', 1.2, 'color',colours(5,:) )
hold on
loglog(gpu_lib1(:,2) , lib1 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(3,:), 'linewidth', 1.2, 'color',colours(3,:) )
loglog(gpu_lib6(:,2) , lib6 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours( 1,:), 'linewidth', 1.2, 'color',colours(1,:) )
loglog(gpu_lib12(:,2) , lib12 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours( 2,:), 'linewidth', 1.2, 'color',colours(2,:) )
loglog(gpu_lib24(:,2) , lib24 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours( 4,:), 'linewidth', 1.2, 'color',colours(4,:) )
%semilogx(gpu_16h(:,2) , Gfs_16h ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(1,:) )
hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('GFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('GPU1 - Sequential','\texttt{DGEMM} 1 thread' , '\texttt{DGEMM} 6 thread' , '\texttt{DGEMM} 12 thread' , '\texttt{DGEMM} 24 thread' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpu1vslib','epsc')
