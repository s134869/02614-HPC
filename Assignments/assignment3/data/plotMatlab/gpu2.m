clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;

gpu_8 = importdata('../gpu2_8.dat',delimiterIn,headerlinesIn);
gpu_8 = gpu_8.data;

gpu_16 = importdata('../gpu2_16.dat',delimiterIn,headerlinesIn);
gpu_16 = gpu_16.data;

gpu_32 = importdata('../gpu2_32.dat',delimiterIn,headerlinesIn);
gpu_32 = gpu_32.data;

gpu_lib = importdata('../lib_L.dat',delimiterIn,headerlinesIn);
gpu_lib = gpu_lib.data;

% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000];
mem = 2*N.^3';

Gfs_8 = mem ./ gpu_8(:,1) / 10^9;
Gfs_16 = mem./ gpu_16(:,1) / 10^9;

Gfs_32 = mem ./ gpu_32(:,1) / 10^9;

Gfs_lib = mem ./ gpu_lib(:,1) / 10^9;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(gpu_8(:,2) , Gfs_8 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
semilogx(gpu_16(:,2) , Gfs_16 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
semilogx(gpu_32(:,2) , Gfs_32 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )
semilogx(gpu_lib(:,2) , Gfs_lib ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(6,:), 'linewidth', 1.2, 'color',colours(6,:) )

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('GFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('8' , '16' , '32' , '\texttt{DGEMM()}' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpu2vslib','epsc')
