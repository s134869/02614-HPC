clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;


gpu_h2 = importdata('../gpu4_h2.dat',delimiterIn,headerlinesIn);
gpu_h2 = gpu_h2.data;

gpu_v2 = importdata('../gpu4_v2.dat',delimiterIn,headerlinesIn);
gpu_v2 = gpu_v2.data;

gpu_h4 = importdata('../gpu4_h4.dat',delimiterIn,headerlinesIn);
gpu_h4 = gpu_h4.data;

gpu_v4 = importdata('../gpu4_v4.dat',delimiterIn,headerlinesIn);
gpu_v4 = gpu_v4.data;

gpu_h8 = importdata('../gpu4_h8.dat',delimiterIn,headerlinesIn);
gpu_h8 = gpu_h8.data;

gpu_v8 = importdata('../gpu4_v8.dat',delimiterIn,headerlinesIn);
gpu_v8 = gpu_v8.data;

% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000];
mem = 2*N.^3';

Gfs_2h = mem ./ gpu_h2(:,2) / 10^12;
Gfs_2v = mem./ gpu_v2(:,2) / 10^12;

Gfs_4h = mem ./ gpu_h4(:,2) / 10^12;
Gfs_4v = mem./ gpu_v4(:,2) / 10^12;

Gfs_8h = mem ./ gpu_h8(:,2) / 10^12;
Gfs_8v = mem./ gpu_v8(:,2) / 10^12;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
loglog(gpu_h2(:,1) , gpu_h2(:,2)/10^3 ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(4,:) )
hold on
plot(gpu_v2(:,1) , gpu_v2(:,2)/10^3 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(4,:), 'linewidth', 1.2, 'color',colours(4,:) )
plot(gpu_h4(:,1) , gpu_h4(:,2)/10^3 ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(1,:) )
plot(gpu_v4(:,1) , gpu_v4(:,2)/10^3 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:)); 
plot(gpu_h8(:,1) , gpu_h8(:,2)/10^3 ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(5,:) )
plot(gpu_v8(:,1) , gpu_v8(:,2)/10^3 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )
hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('TFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('H2' , 'V2' , 'H4' , 'V4' , 'H8' , 'V8' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpu4','epsc')

%%
% plot(Nthreads , T_mandel , '-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:));
% plot(Nthreads , T_3noOpt ,  '-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(2,:));

