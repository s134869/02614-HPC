clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;

gpu5_16 = importdata('../gpu5_16.dat',delimiterIn,headerlinesIn);
gpu5_16 = gpu5_16.data;

gpu5_32 = importdata('../gpu5_32.dat',delimiterIn,headerlinesIn);
gpu5_32 = gpu5_32.data;

gpu5_8 = importdata('../gpu5_8.dat',delimiterIn,headerlinesIn);
gpu5_8 = gpu5_8.data;


% Compute Gflops
N = [960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480];
mem = 2*N.^3';
Gfs_16 = mem ./ gpu5_16(:,1) / 10^12;
Gfs_32 = mem ./ gpu5_32(:,1) / 10^12;
Gfs_8 = mem ./ gpu5_8(:,1) / 10^12;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(gpu5_8(:,2) , Gfs_8 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
semilogx(gpu5_16(:,2) , Gfs_16 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
semilogx(gpu5_32(:,2) , Gfs_32 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )



grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('TFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('Block size 8' , 'Block size 16' , 'Block size 32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southwest ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpu5','epsc')


