clear all
clc
close all 

IT = 1000;

delimiterIn = ',';
headerlinesIn = 1;

omp = importdata('../poisson_omp_L.dat',delimiterIn,headerlinesIn);
omp = omp.data;

gpu8 = importdata('../poisson2_8.dat',delimiterIn,headerlinesIn);
gpu8 = gpu8.data;

gpu16 = importdata('../poisson2_16.dat',delimiterIn,headerlinesIn);
gpu16 = gpu16.data;

gpu32 = importdata('../poisson2_32.dat',delimiterIn,headerlinesIn);
gpu32 = gpu32.data;


% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000]';
flop = 11*N.^2 * IT;


Gfs_omp = flop ./ omp(:,2) / 10^9;
Gfs_gpu8 = flop./ gpu8(:,2) / 10^9;
Gfs_gpu16 = flop./ gpu16(:,2) / 10^9;
Gfs_gpu32 = flop./ gpu32(:,2) / 10^9;


colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(omp(:,1) , Gfs_omp ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
semilogx(gpu8(:,1) , Gfs_gpu8 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
semilogx(gpu16(:,1) , Gfs_gpu16 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(3,:), 'linewidth', 1.2, 'color',colours(3,:) )
semilogx(gpu32(:,1) , Gfs_gpu32 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )

hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('GFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('OpenMP' , 'Naive GPU Blocksize 8' , 'Naive GPU Blocksize 16' , 'Naive GPU Blocksize 32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' best' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'pois8','epsc')


%% Bandwidth

% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000]';
band = 3*N.^2 * 8 * IT;

ach_omp = band./ omp(:,2) / 10^9 / 128 * 100;
ach_gpu8 = band./ gpu8(:,2) / 10^9 / 900 * 100;
ach_gpu16 = band./ gpu16(:,2) / 10^9 / 900 * 100;
ach_gpu32 = band./ gpu32(:,2) / 10^9 / 900 * 100;

%%
close all
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])

semilogx(omp(:,1) , Gfs_omp ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:) )
hold on
semilogx(gpu8(:,1) , ach_gpu8 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(2,:), 'linewidth', 1.2, 'color',colours(2,:) )
semilogx(gpu16(:,1) , ach_gpu16 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(3,:), 'linewidth', 1.2, 'color',colours(3,:) )
semilogx(gpu32(:,1) , ach_gpu32 ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )

hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('Memory utilization [\%]', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('OpenMP' , 'Naive GPU Blocksize 8' , 'Naive GPU Blocksize 16' , 'Naive GPU Blocksize 32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' best' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'pois8_band','epsc')