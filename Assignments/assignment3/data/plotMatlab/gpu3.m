clear all
clc
close all 

delimiterIn = ',';
headerlinesIn = 1;


gpu_16h = importdata('../gpu3_h_16.dat',delimiterIn,headerlinesIn);
gpu_16h = gpu_16h.data;

gpu_16v = importdata('../gpu3_v_16.dat',delimiterIn,headerlinesIn);
gpu_16v = gpu_16v.data;

gpu_8v = importdata('../gpu3_v_8.dat',delimiterIn,headerlinesIn);
gpu_8v = gpu_8v.data;

gpu_8h = importdata('../gpu3_h_8.dat',delimiterIn,headerlinesIn);
gpu_8h = gpu_8h.data;

gpu_32h = importdata('../gpu3_h_32.dat',delimiterIn,headerlinesIn);
gpu_32h = gpu_32h.data;

gpu_32v = importdata('../gpu3_v_32.dat',delimiterIn,headerlinesIn);
gpu_32v = gpu_32v.data;

% Compute Gflops
N = [1000 2000 3000 4000 5000 6000 7000 8000 9000 10000];
mem = 2*N.^3';

Gfs_16h = mem ./ gpu_16h(:,1) / 10^12;
Gfs_16v = mem./ gpu_16v(:,1) / 10^12;

Gfs_8h = mem ./ gpu_8h(:,1) / 10^12;
Gfs_8v = mem./ gpu_8v(:,1) / 10^12;

Gfs_32h = mem ./ gpu_32h(:,1) / 10^12;
Gfs_32v = mem./ gpu_32v(:,1) / 10^12;

colours = permute(get(gca, 'colororder'), [1 3 2]);

%%
close all
figure('Renderer', 'painters', 'Position', [10 10 1200 800])
semilogx(gpu_8h(:,2) , Gfs_8h ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(4,:) )
hold on
semilogx(gpu_8v(:,2) , Gfs_8v ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(4,:), 'linewidth', 1.2, 'color',colours(4,:) )
semilogx(gpu_16h(:,2) , Gfs_16h ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(1,:) )
semilogx(gpu_16h(:,2) , Gfs_16v ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:)); 
semilogx(gpu_32h(:,2) , Gfs_32h ,'-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(5,:) )
semilogx(gpu_32v(:,2) , Gfs_32v ,'-gs' , 'markersize' , 7,'MarkerFaceColor', colours(5,:), 'linewidth', 1.2, 'color',colours(5,:) )
hold off

grid on
xlabel('Memory Footprint [kB]' , 'FontSize' , 25, 'Interpreter' , 'LaTex')
ylabel('TFlops/s', 'FontSize' , 25, 'Interpreter' , 'LaTex')
leg = legend('H8' , 'V8' , 'H16' , 'V16' , 'H32' , 'V32' )
set( leg  ,  'Fontsize'  ,   25 ,  'location'  ,  ' southeast ' , 'Interpreter' , 'latex')
set(gca,'FontSize',25)

saveas(gcf,'gpu3','epsc')

%%
plot(Nthreads , T_mandel , '-gs' , 'markersize' , 7,'MarkerFaceColor', colours(1,:), 'linewidth', 1.2, 'color',colours(1,:));
plot(Nthreads , T_3noOpt ,  '-gs' , 'markersize' , 7, 'linewidth', 1.2, 'color',colours(2,:));

