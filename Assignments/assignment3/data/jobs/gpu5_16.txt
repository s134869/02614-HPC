Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-14>
Subject: Job 1867073: <gpu5_16> in cluster <dcc> Exited

Job <gpu5_16> was submitted from host <n-62-13-2> by user <s144257> in cluster <dcc> at Thu Jan 24 16:47:46 2019
Job was executed on host(s) <n-62-20-14>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 16:47:47 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 16:47:47 2019
Terminated at Thu Jan 24 16:48:07 2019
Results reported at Thu Jan 24 16:48:07 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_16
#BSUB -r 
#BSUB -R "rusage[mem=3GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_16.dat
# output=data/gpu2_32.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2}'>>$output
done

exit 0
------------------------------------------------------------

TERM_MEMLIMIT: job killed after reaching LSF memory usage limit.
Exited with signal termination: 25.

Resource usage summary:

    CPU time :                                   18.20 sec.
    Max Memory :                                 3072 MB
    Average Memory :                             2211.33 MB
    Total Requested Memory :                     3072.00 MB
    Delta Memory :                               0.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   23 sec.
    Turnaround time :                            21 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-16>
Subject: Job 1867534: <gpu5_16> in cluster <dcc> Done

Job <gpu5_16> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 20:26:38 2019
Job was executed on host(s) <n-62-20-16>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 20:26:39 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 20:26:39 2019
Terminated at Thu Jan 24 20:26:47 2019
Results reported at Thu Jan 24 20:26:47 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_16
#BSUB -r 
#BSUB -R "rusage[mem=3GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_32.dat
# output=data/gpu2_16.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   4.73 sec.
    Max Memory :                                 572 MB
    Average Memory :                             442.00 MB
    Total Requested Memory :                     3072.00 MB
    Delta Memory :                               2500.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   12 sec.
    Turnaround time :                            9 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-16>
Subject: Job 1867538: <gpulib_16> in cluster <dcc> Exited

Job <gpulib_16> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 20:27:11 2019
Job was executed on host(s) <n-62-20-16>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 20:27:13 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 20:27:13 2019
Terminated at Thu Jan 24 20:27:49 2019
Results reported at Thu Jan 24 20:27:49 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpulib_16
#BSUB -r 
#BSUB -R "rusage[mem=3GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpulib_1.dat
# output=data/gpu2_32.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpulib $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

TERM_MEMLIMIT: job killed after reaching LSF memory usage limit.
Exited with signal termination: 25.

Resource usage summary:

    CPU time :                                   32.00 sec.
    Max Memory :                                 3072 MB
    Average Memory :                             2106.00 MB
    Total Requested Memory :                     3072.00 MB
    Delta Memory :                               0.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   57 sec.
    Turnaround time :                            38 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 1000
Running for n = 2000
Running for n = 3000
Running for n = 4000
Running for n = 5000
Running for n = 6000
Running for n = 7000
Running for n = 8000
Running for n = 9000
Running for n = 10000

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-15>
Subject: Job 1867561: <gpulib_16> in cluster <dcc> Done

Job <gpulib_16> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 20:36:46 2019
Job was executed on host(s) <n-62-20-15>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 20:36:46 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 20:36:46 2019
Terminated at Thu Jan 24 20:37:08 2019
Results reported at Thu Jan 24 20:37:08 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpulib_16
#BSUB -r 
#BSUB -R "rusage[mem=4GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"
output=data/gpulib_1.dat
# output=data/gpu2_32.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpulib $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   19.00 sec.
    Max Memory :                                 356 MB
    Average Memory :                             356.00 MB
    Total Requested Memory :                     4096.00 MB
    Delta Memory :                               3740.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   27 sec.
    Turnaround time :                            22 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-15>
Subject: Job 1867562: <gpu5_16> in cluster <dcc> Done

Job <gpu5_16> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 20:37:13 2019
Job was executed on host(s) <n-62-20-15>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 20:37:13 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 20:37:13 2019
Terminated at Thu Jan 24 20:38:32 2019
Results reported at Thu Jan 24 20:38:32 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_16
#BSUB -r 
#BSUB -R "rusage[mem=4GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_32.dat
# output=data/gpu2_16.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   76.00 sec.
    Max Memory :                                 2398 MB
    Average Memory :                             1822.80 MB
    Total Requested Memory :                     4096.00 MB
    Delta Memory :                               1698.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   79 sec.
    Turnaround time :                            79 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-15>
Subject: Job 1867610: <gpu5_8> in cluster <dcc> Done

Job <gpu5_8> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 20:57:39 2019
Job was executed on host(s) <n-62-20-15>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 20:57:39 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 20:57:39 2019
Terminated at Thu Jan 24 21:00:32 2019
Results reported at Thu Jan 24 21:00:32 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_8
#BSUB -r 
#BSUB -R "rusage[mem=4GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_8.dat
# output=data/gpu2_16.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   169.17 sec.
    Max Memory :                                 2396 MB
    Average Memory :                             1737.29 MB
    Total Requested Memory :                     4096.00 MB
    Delta Memory :                               1700.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   199 sec.
    Turnaround time :                            173 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-16>
Subject: Job 1867717: <gpu5_16> in cluster <dcc> Done

Job <gpu5_16> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 21:45:08 2019
Job was executed on host(s) <n-62-20-16>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 21:45:09 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 21:45:09 2019
Terminated at Thu Jan 24 21:45:38 2019
Results reported at Thu Jan 24 21:45:38 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_16
#BSUB -r 
#BSUB -R "rusage[mem=4GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_16.dat
# output=data/gpu2_16.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   26.32 sec.
    Max Memory :                                 1262 MB
    Average Memory :                             947.00 MB
    Total Requested Memory :                     4096.00 MB
    Delta Memory :                               2834.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   54 sec.
    Turnaround time :                            30 sec.

The output (if any) is above this job summary.

Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 960
Running for n = 2112
Running for n = 3264
Running for n = 4416
Running for n = 5568
Running for n = 6720
Running for n = 7872
Running for n = 9024
Running for n = 10176
Running for n = 11328
Running for n = 12480

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-20-15>
Subject: Job 1867720: <gpu5_16> in cluster <dcc> Done

Job <gpu5_16> was submitted from host <n-62-20-10> by user <s144257> in cluster <dcc> at Thu Jan 24 21:46:03 2019
Job was executed on host(s) <n-62-20-15>, in queue <hpcintrogpu>, as user <s144257> in cluster <dcc> at Thu Jan 24 21:46:03 2019
</zhome/e7/a/97980> was used as the home directory.
</zhome/e7/a/97980/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 21:46:03 2019
Terminated at Thu Jan 24 21:47:21 2019
Results reported at Thu Jan 24 21:47:21 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_16
#BSUB -r 
#BSUB -R "rusage[mem=6GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_16.dat
# output=data/gpu2_16.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   76.06 sec.
    Max Memory :                                 2396 MB
    Average Memory :                             1820.80 MB
    Total Requested Memory :                     6144.00 MB
    Delta Memory :                               3748.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                8
    Run time :                                   99 sec.
    Turnaround time :                            78 sec.

The output (if any) is above this job summary.

