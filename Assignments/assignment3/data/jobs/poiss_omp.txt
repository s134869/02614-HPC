Loaded module: cuda/10.0
Loaded dependency [gcc/7.3.0]: binutils
Loaded module: gcc/7.3.0
Running for n = 50
Running for n = 100
Running for n = 200
Running for n = 300
Running for n = 400
Running for n = 500
Running for n = 600
Running for n = 700
Running for n = 800
Running for n = 900
Running for n = 1000
Running for n = 1000
Running for n = 2000
Running for n = 3000
Running for n = 4000
Running for n = 5000
Running for n = 6000
Running for n = 7000
Running for n = 8000
Running for n = 9000
Running for n = 10000

------------------------------------------------------------
Sender: LSF System <lsfadmin@n-62-31-19>
Subject: Job 1867723: <poissomp> in cluster <dcc> Done

Job <poissomp> was submitted from host <n-62-20-13> by user <s144470> in cluster <dcc> at Thu Jan 24 21:46:20 2019
Job was executed on host(s) <16*n-62-31-19>, in queue <hpcintro>, as user <s144470> in cluster <dcc> at Thu Jan 24 21:46:20 2019
</zhome/56/b/98193> was used as the home directory.
</zhome/56/b/98193/Desktop/02614-HPC/Assignments/assignment3> was used as the working directory.
Started at Thu Jan 24 21:46:20 2019
Terminated at Thu Jan 24 21:48:41 2019
Results reported at Thu Jan 24 21:48:41 2019

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#!/bin/sh
#BSUB -W 00:15
#BSUB -J poissomp
#BSUB -r 
#BSUB -q hpcintro
#BSUB -R "rusage[mem=10GB]"
#BSUB -R "span[hosts=1]"
#BSUB -n 16
#BSUB -o data/jobs/poiss_omp.txt

module load cuda/10.0
module load gcc/7.3.0

S="50 100 200 300 400 500 600 700 800 900 1000"
L="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"
output1=data/poisson_omp_S.dat
output2=data/poisson_omp_L.dat

# Make header and flush file contents
echo "memory,time">$output1
echo "memory,time">$output2

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $S
do
    echo "Running for n = $n";
    OMP_NUM_THREADS=16 ./poisson-omp/poisson 6 $n 1000 1 | awk '{print $1}'>>$output1
done

for n in $L
do
    echo "Running for n = $n";
    OMP_NUM_THREADS=16 ./poisson-omp/poisson 6 $n 1000 1 | awk '{print $1}'>>$output2
done

exit 0


------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1399.90 sec.
    Max Memory :                                 2292 MB
    Average Memory :                             1529.83 MB
    Total Requested Memory :                     163840.00 MB
    Delta Memory :                               161548.00 MB
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                21
    Run time :                                   152 sec.
    Turnaround time :                            141 sec.

The output (if any) is above this job summary.

