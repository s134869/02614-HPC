#!/bin/sh
#BSUB -W 20:00
#BSUB -J matgpu1
#BSUB -r 
#BSUB -R "rusage[mem=4GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu1.txt

module load cuda/10.0
module load gcc/7.3.0

N=" 1000"

output1=data/gpu1_small.dat

# Make header and flush file contents
#echo "memory,mflops">$output1


if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi


MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu1 1000 1000 1000 | awk '{print $1","$2","$3}'>>$output1


exit 0

