#!/bin/sh
#BSUB -W 00:10
#BSUB -J matgpu4
#BSUB -r 
#BSUB -R "rusage[mem=3GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu4.txt

module load cuda/10.0
module load gcc/7.3.0

output3=data/lib_L.dat

MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc lib 10000 10000 10000 | awk '{ print $1","$2}'>>$output3


exit 0

