#!/bin/sh
#BSUB -W 00:20
#BSUB -J poiss1
#BSUB -r 
#BSUB -R "rusage[mem=5GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/poiss1.txt

module load cuda/10.0
module load gcc/7.3.0

S="50 100 200 300 400 500 600 700 800 900 1000"
# output1=data/poisson1_32.dat
# output1=data/poisson1_16.dat
output1=data/poisson1_8.dat

# Make header and flush file contents
echo "memory,time">$output1

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $S
do
    echo "Running for n = $n";
    ./poisson/poiss 1 $n 1000 | awk '{ print $1","$2}'>>$output1
done

exit 0

