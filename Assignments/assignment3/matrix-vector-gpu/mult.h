__global__ void mxv(int m, int n, double *a, double *b, double *c);
__global__ void mxv_columns(int m, int n, double *A, double *b, double *c);
__global__ void mxv_grid(int m, int n, double *A, double *b, double *c);