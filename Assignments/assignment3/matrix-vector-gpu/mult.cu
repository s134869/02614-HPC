#include <stdio.h>
#include <helper_cuda.h>

__global__ void mxv(int m, int n, double *A, double *b, double *c)
{
    int i, j;
    i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < m)
    {
        for (j = 0; j < n; j++)
            c[i] += A[i * n + j] * b[j];
    }
}

__global__ void mxv_columns(int m, int n, double *A, double *b, double *c)
{
    int i, j;
    j = blockIdx.x * blockDim.x + threadIdx.x;
    if (j < n)
    {
        for (i = 0; i < m; i++)
            atomicAdd(&c[i], A[i * n + j] * b[j]);
    }
}

__global__ void mxv_grid(int m, int n, double *A, double *b, double *c)
{
    int i, j;
    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;
    if (i < m && j < n)
        atomicAdd(&c[i], A[i * n + j] * b[j]);
}
