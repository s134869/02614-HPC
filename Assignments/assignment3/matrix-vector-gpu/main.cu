#include <stdio.h>
#include <stdlib.h>
#include "mult.h"
#include <helper_cuda.h>
#include <omp.h>
#include "datatools.h"

#define DEFAULT_BLOCK_SIZE 32

// void call_cublas()
// {
//     cublasHandle_t handle;

// }

void multi_gpu(int m, int n, long size_A, long size_b,
               long size_c, double *h_A, double *h_b, double *h_c)
{
    // Set gridsize
    int gridWidth = 1 + m / DEFAULT_BLOCK_SIZE;
    int gridHeight = 1 + n / DEFAULT_BLOCK_SIZE;

    double time, elapsed;

    // printf("Grid: %d x %d", gridWidth, gridHeight);
    dim3 dimBlock(DEFAULT_BLOCK_SIZE, DEFAULT_BLOCK_SIZE, 1);
    dim3 dimGrid(gridWidth, gridHeight, 1);

    double *d0_A, *d1_A;
    double *d0_c, *d1_c;
    double *d0_b, *d1_b;

    cudaSetDevice(0);
    cudaMalloc((void **)&d0_A, size_A / 2);
    cudaMalloc((void **)&d0_b, size_b);
    cudaMalloc((void **)&d0_c, size_c / 2);
    cudaMemcpy(d0_A, h_A, size_A / 2, cudaMemcpyHostToDevice);
    cudaMemcpy(d0_b, h_b, size_b, cudaMemcpyHostToDevice);
    cudaMemcpy(d0_c, h_c, size_c / 2, cudaMemcpyHostToDevice);

    cudaSetDevice(1);
    cudaMalloc((void **)&d1_A, size_A / 2);
    cudaMalloc((void **)&d1_b, size_b);
    cudaMalloc((void **)&d1_c, size_c / 2);
    cudaMemcpy(d1_A, h_A + (m * n) / 2, size_A / 2, cudaMemcpyHostToDevice);
    cudaMemcpy(d1_b, h_b, size_b, cudaMemcpyHostToDevice);
    cudaMemcpy(d1_c, h_c + m / 2, size_c / 2, cudaMemcpyHostToDevice);

    int k, loops = 10;
    for (k = 0; k < loops; k++)
    {
        // Start timer
        time = omp_get_wtime();

        // Run on device 0
        cudaSetDevice(0);
        mxv_grid<<<dimGrid, dimBlock>>>(m / 2, n, d0_A, d0_b, d0_c);

        // Run on device 0
        cudaSetDevice(1);
        mxv_grid<<<dimGrid, dimBlock>>>(m / 2, n, d1_A, d1_b, d1_c);

        // Memory transfer
        cudaSetDevice(0);
        cudaDeviceSynchronize();
        cudaMemcpy(h_c, d0_c, size_c / 2, cudaMemcpyDeviceToHost);

        cudaSetDevice(1);
        cudaDeviceSynchronize();
        cudaMemcpy(h_c + m / 2, d1_c, size_c / 2, cudaMemcpyDeviceToHost);

        // Stop timer
        elapsed += omp_get_wtime() - time;
    }

    // print_vec(m, h_c);
    double avg_glops = loops * 2.0 * (double)m * (double)n / elapsed / 1E9;
    double avg_bandwidth = loops * 8 * ((double)m * (double)n + (double)n + (double)m) / elapsed / 1E9;
    printf("%d,%d,%.5f,%.5f\n", m, n, avg_glops, avg_bandwidth);
}

int main(int argc, char *argv[])
{
    int program, m, n, i, j;

    // Read size from input
    m = atoi(argv[1]);
    n = atoi(argv[2]);
    program = atoi(argv[3]);

    double *h_A, *d_A;
    double *h_b, *d_b;
    double *h_c, *d_c;

    // Calculate array sizes
    // 'long' is important for big matrices..!
    long size_A = m * n * sizeof(double);
    long size_b = n * sizeof(double);
    long size_c = m * sizeof(double);

    // Allocate memory
    cudaMallocHost((void **)&h_A, size_A);
    cudaMallocHost((void **)&h_b, size_b);
    cudaMallocHost((void **)&h_c, size_c);

    // Fill out host data
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            h_A[i * n + j] = 1.0;

    for (i = 0; i < n; i++)
        h_b[i] = 1.0;

    for (i = 0; i < m; i++)
        h_c[i] = 0.0;

    if (program == 0) // Multi-GPU program
    {
        multi_gpu(m, n, size_A, size_b, size_c, h_A, h_b, h_c);
    }
    else // Single GPU-program(s)
    {
        cudaMalloc((void **)&d_A, size_A);
        cudaMalloc((void **)&d_b, size_b);
        cudaMalloc((void **)&d_c, size_c);

        // Transfer host data data to device
        cudaMemcpy(d_A, h_A, size_A, cudaMemcpyHostToDevice);
        cudaMemcpy(d_b, h_b, size_b, cudaMemcpyHostToDevice);
        cudaMemcpy(d_c, h_c, size_c, cudaMemcpyHostToDevice);

        // Start timer
        double time;

        int k, loops = 10;
        int grid_size;

        if (program == 1) // Row-wise
        {
            grid_size = 1 + m / DEFAULT_BLOCK_SIZE;
            time = omp_get_wtime();
            for (k = 0; k < loops; k++)
                mxv<<<grid_size, DEFAULT_BLOCK_SIZE>>>(m, n, d_A, d_b, d_c);
        }
        else if (program == 2) // Column-wise
        {
            grid_size = 1 + n / DEFAULT_BLOCK_SIZE;
            time = omp_get_wtime();
            for (k = 0; k < loops; k++)
                mxv_columns<<<grid_size, DEFAULT_BLOCK_SIZE>>>(m, n, d_A, d_b, d_c);
        }
        else if (program == 3) // Grid-wise
        {
            // Set gridsize
            int gridWidth = 1 + m / DEFAULT_BLOCK_SIZE;
            int gridHeight = 1 + n / DEFAULT_BLOCK_SIZE;

            // printf("Grid: %d x %d", gridWidth, gridHeight);
            dim3 dimBlock(DEFAULT_BLOCK_SIZE, DEFAULT_BLOCK_SIZE, 1);
            dim3 dimGrid(gridWidth, gridHeight, 1);
            time = omp_get_wtime();
            for (k = 0; k < loops; k++)
                mxv_grid<<<dimGrid, dimBlock>>>(m, n, d_A, d_b, d_c);
        }

        // Synchronize
        cudaDeviceSynchronize();

        // End timer
        double elapsed = omp_get_wtime() - time;

        // Transfer data from device to host
        cudaMemcpy(h_A, d_A, size_A, cudaMemcpyDeviceToHost);
        cudaMemcpy(h_b, d_b, size_b, cudaMemcpyDeviceToHost);
        cudaMemcpy(h_c, d_c, size_c, cudaMemcpyDeviceToHost);

        double avg_glops = loops * 2.0 * (double)m * (double)n / elapsed / 1E9;
        double avg_bandwidth = loops * 8.0 * ((double)m * (double)n + (double)n + (double)m) / elapsed / 1E9;
        // print_vec(m, h_c);
        printf("%d,%d,%.5f,%.5f\n", m, n, avg_glops, avg_bandwidth);

        cudaFreeHost(h_A);
        cudaFreeHost(h_b);
        cudaFreeHost(h_c);
        cudaFree(d_A);
        cudaFree(d_b);
        cudaFree(d_c);
    }

    return 0;
}
