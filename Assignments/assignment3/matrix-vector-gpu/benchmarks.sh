N="200 400 600 800 1000 1500 2000 4000 6000 8000 10000 15000 20000"
multi_file=data/multi_gpu.dat
row_file=data/rows.dat
col_file=data/cols.dat
grid_file=data/grid.dat

echo "n,m,gflops,bandwidth"> $multi_file
echo "n,m,gflops,bandwidth"> $row_file
echo "n,m,gflops,bandwidth"> $col_file
echo "n,m,gflops,bandwidth"> $grid_file

for n in $N
do
    echo "Running for n = $n";
	./mvx $n $n 0 >> $multi_file
	./mvx $n $n 1 >> $row_file
	./mvx $n $n 2 >> $col_file
    ./mvx $n $n 3 >> $grid_file
done

echo "Done."

