
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>

#include "datatools.h"

void print_matrix(int m, int n, double *M) {
    int i, j;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            printf("%1.f\t ", M[i*n+j]);
        }
        printf("\n");
    }
}

void print_vec(int n, double *v) {
    int i;
    printf("\nVector: [ ");
    for (i = 0; i < n; i++)
        printf("%1.f\t ", v[i]);
    printf("]\n");
}
