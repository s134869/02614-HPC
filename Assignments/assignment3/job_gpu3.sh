#!/bin/sh
#BSUB -W 00:10
#BSUB -J matgpu4
#BSUB -r 
#BSUB -R "rusage[mem=3GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu4.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"
# output=data/gpu3_32_h.dat
# output=data/gpu3_32_v.dat
# output=data/gpu3_16_h.dat
# output=data/gpu3_16_v.dat
# output=data/gpu3_8_v.dat
output=data/gpu3_8_h.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu3 $n $n $n | awk '{ print $1","$2}'>>$output
done

exit 0

