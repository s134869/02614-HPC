N="100 200 300 400 500 1000 1500"
gpu1_file=data/gpu1.dat
lib_file=data/lib.dat
# Make header and flush file contents
echo "memory,mflops">$gpu1_file
echo "memory,mflops">$lib_file
 
for n in $N
do
    m=$n+1
    k=$n+3
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu1 $m $n $k | awk '{ print $1","$2}'>>$gpu1_file
    MKL_NUM_THREADS=1 MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc lib $m $n $k | awk '{ print $1","$2}'>>$lib_file
done
 
echo "Done."