#!/bin/sh
#BSUB -W 00:10
#BSUB -J matgpu4
#BSUB -r 
#BSUB -R "rusage[mem=4GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu4.txt

module load cuda/10.0
module load gcc/7.3.0

N="100 200 500 1000 5000 10000"
output1=data/gpu2.dat
output2=data/gpu3.dat
output3=data/gpu4_h2.dat

# Make header and flush file contents
echo "memory,mflops">$output1
echo "memory,mflops">$output2
echo "memory,mflops">$output3

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu2 $n $n $n | awk '{ print $1","$2}'>>$output1
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu3 $n $n $n | awk '{ print $1","$2}'>>$output2
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu4 $n $n $n | awk '{ print $1","$2}'>>$output3
done

exit 0

