N = 10000;
delta = 1 / (N-1); 
x = -1:delta:1;
y = -1:delta:1;
[X,Y] = meshgrid(x,y);
Z = sin(X)+cos(Y);
contour(X,Y,Z)