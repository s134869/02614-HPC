void jacobi_seq(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta);

void gauss_seidel(int N , double **U , double **f , int max_iter , double threshold , double delta);

void jacobi_omp(int N , double **U_old , double **U , double **f , int max_iter , double threshold , double delta);

void jacobi_omp_gpu(int N , double *U_old , double *U , double *f , int max_iter , double threshold , double delta);

void jacobi_omp_simple(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta);

void jacobi_omp_with_races(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta);

void jacobi_omp_simple_scheduling(int N, double **U_old, double **U, double **f, int max_iter, double threshold, double delta);

__global__ void jacobi_omp_multi_kernel(int N, double *U_old, double *U, double *f, double delta);

__global__ void jacobi_omp_single_kernel(int N, double *U_old, double *U, double *f, double delta);

__global__ void jacobi_omp_multi_gpu_kernel0(int N, double *U0_old, double *U1_old, double *U, double *f, double delta);
__global__ void jacobi_omp_multi_gpu_kernel1(int N, double *U0_old, double *U1_old, double *U, double *f, double delta);

