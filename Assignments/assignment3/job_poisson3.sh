#!/bin/sh
#BSUB -W 00:10
#BSUB -J poiss3
#BSUB -r 
#BSUB -R "rusage[mem=5GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=2:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/poiss2.txt

module load cuda/10.0
module load gcc/7.3.0

L="1000 2000 3000 4000 5000 6000 7000 8000 9000 10000"
# output1=data/poisson3_32.dat
# output1=data/poisson3_16.dat
output1=data/poisson3_8.dat

# Make header and flush file contents
echo "memory,time">$output1

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $L
do
    echo "Running for n = $n";
    ./poisson/poiss 3 $n 1000 | awk '{ print $1","$2}'>>$output1
done

exit 0

