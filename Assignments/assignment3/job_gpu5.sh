#!/bin/sh
#BSUB -W 00:10
#BSUB -J gpu5_16
#BSUB -r 
#BSUB -R "rusage[mem=6GB]"
#BSUB -q hpcintrogpu
#BSUB -gpu "num=1:mode=exclusive_process:mps=yes"
#BSUB -o data/jobs/gpu5_16.txt

module load cuda/10.0
module load gcc/7.3.0

# N="50 100 200 300 400 500 600 700 800 900 1000"
N="960 2112 3264 4416 5568 6720 7872 9024 10176 11328 12480"
output=data/gpu5_16.dat
# output=data/gpu2_16.dat

# Make header and flush file contents
echo "memory,mflops">$output

if [ ! -d data/jobs ]; then
  mkdir -p  data/jobs;
fi

for n in $N
do
    echo "Running for n = $n";
    MFLOPS_MAX_IT=1 MATMULT_COMPARE=0 ./matmult_f.nvcc gpu5 $n $n $n | awk '{ print $1","$2 ","$3}'>>$output
done

exit 0